<%--
  Created by IntelliJ IDEA.
  User: lenovo
  Date: 2021/3/15
  Time: 22:23
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/lib/layui-v2.5.5/css/layui.css">
    <script src="${pageContext.request.contextPath}/webjars/jquery/3.4.1/jquery.min.js"></script>
    <!-- 注意：如果你直接复制所有代码到本地，上述css路径需要改成你本地的 -->
</head>
<body>
<div class="site-text" style="margin: 5%;">
    <form class="layui-form" id="detailTopic" lay-filter="detailTopic">
        <div class="layui-form-item">
            <label class="layui-form-label">课题名称</label>
            <div class="layui-input-block">
                <input type="text" id="thesisName" name="thesisName" lay-verify="title" autocomplete="off"
                       placeholder="请输入标题" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item" class="layui-form">
            <label class="layui-form-label">分配学生</label>
            <div class="layui-input-block">
                <select id="s_id" name="s_id" lay-filter="s_id">
                    <option value=""></option>
                </select>
            </div>
        </div>
        <%--<div class="layui-form-item">
            <label class="layui-form-label">任务书名称</label>
            <div class="layui-input-block">
                <input type="text" id="taskBookName" name="taskBookName" lay-verify="title" autocomplete="off"
                       placeholder="请输入标题" class="layui-input">
            </div>
        </div>--%>
    </form>
</div>
<script src="${pageContext.request.contextPath}/static/lib/layui-v2.5.5/layui.js" charset="utf-8"></script>
<!-- 注意：如果你直接复制所有代码到本地，上述js路径需要改成你本地的 -->
<script>
    layui.use(['form','jquery'], function() {
        const form = layui.form
            , layer = layui.layer
            , $ = layui.jquery;
        form.val("detailTopic", {
            "id":parent.id,
            "taskBookName": parent.taskBookName,
            "thesisName":parent.thesisName,
        })
        var s_id = parent.s_id;
        //分配学生
        $.ajax({
            url: '${pageContext.request.contextPath}/getMyStudentList',//获取学院列表信息
            dataType: 'json',
            type: 'get',
            success: function (data) {
                //使用循环遍历，给下拉列表赋值
                $.each(data.data, function (index, value) {
                    $('#s_id').append(new Option(value.studentName,value.id));// 下拉菜单里添加元素
                    $('#s_id').find('option').each(function() {
                        $(this).attr('selected', $(this).val() == s_id);
                    });
                });
                layui.form.render("select");//重新渲染 固定写法
            }
        })
    })
</script>
</body>
</html>
