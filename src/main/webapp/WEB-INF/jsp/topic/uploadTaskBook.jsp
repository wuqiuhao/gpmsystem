<%--
  Created by IntelliJ IDEA.
  User: lenovo
  Date: 2021/3/15
  Time: 9:07
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>上传任务书</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/lib/layui-v2.5.5/css/layui.css">
    <script src="${pageContext.request.contextPath}/webjars/jquery/3.4.1/jquery.min.js"></script>
    <!-- 注意：如果你直接复制所有代码到本地，上述css路径需要改成你本地的 -->
</head>
<body>
<div class="site-text" style="margin: 5%;">
    <form class="layui-form" id="uploadTaskBook" lay-filter="uploadTaskBook">
        <div class="layui-form-item" style="display: none">
            <label class="layui-form-label">ID</label>
            <div class="layui-input-block">
                <input type="text" id="id" name="id" lay-verify="title" autocomplete="off"
                      class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">任务书名称</label>
            <div class="layui-input-block">
                <input type="text" id="taskBookName" name="taskBookName" lay-verify="title" autocomplete="off"
                       placeholder="请输入任务书名称" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item" style="display: none">
            <label class="layui-form-label">任务书地址</label>
            <div class="layui-input-block">
                <input type="text" id="taskBookPath" name="taskBookPath" lay-verify="title" autocomplete="off"
                       class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">上传任务书</label>
            <div class="layui-input-block">
                <button type="button" class="layui-btn" id="selectFile"><i class="layui-icon"></i>选择文件</button>
                <button type="button" class="layui-btn" id="upload">开始上传</button>
            </div>
        </div>
        <div class="layui-form-item" style="margin-top:40px">
            <div class="layui-input-block">
                <button lay-submit="" id="uploadTask" class="layui-btn layui-btn-normal layui-btn-radius">上传任务书</button>
            </div>
        </div>
    </form>
</div>
<script src="${pageContext.request.contextPath}/static/lib/layui-v2.5.5/layui.js" charset="utf-8"></script>
<!-- 注意：如果你直接复制所有代码到本地，上述js路径需要改成你本地的 -->
<script>
    layui.use(['form','upload'], function() {
        const form = layui.form
            , layer = layui.layer
            ,upload = layui.upload
            , $ = layui.jquery;
        //指定文件上传及上传类型
        upload.render({ //允许上传的文件后缀
            elem: '#selectFile'
            ,url: '${pageContext.request.contextPath}/uploadSubmit?type=taskbook' //改成您自己的上传接口
            ,accept: 'file' //普通文件
            ,exts: 'docx|doc' //只允许上传word
            ,auto: false
            //,multiple: true
            ,bindAction: '#upload'
            //上传前的回调
            ,before: function () {
                this.data = {
                    type: 'taskbook',
                    id:parent.id,
                    grade:parent.grade,
                }
            }
            //操作成功的回调
            ,done: function (res, index, upload) {
                if (res.code === 1){
                    $('#taskBookPath').val(res.data);
                    layer.msg('上传成功!')
                }else {
                    layer.msg('上传失败!')
                }
            }
        });
        form.val("uploadTaskBook", {
            "id":parent.id,
            "taskBookName":parent.thesisName,
        })
        //监听submit提交按钮 button ，lay-filter 为 noticeAdd 的
        $('#uploadTask').on('click',function (){
            $.ajax({
                type: "POST",
                dataType: "text",
                url: "${pageContext.request.contextPath}/updateTopic",
                data: $('#uploadTaskBook').serialize(),  //表单数据
                success: function (result) {
                    result = JSON.parse(result);
                    if (result === "1") {
                        layer.msg('添加成功，1秒后自动关闭该窗口');
                        //延迟1秒执行，目的是让用户看到提示
                        setTimeout(function () {
                            //1、先得到当前iframe层（弹出层）的索引  ///2、提交成功关闭弹出层窗口
                            const index = parent.layer.getFrameIndex(window.name);
                            parent.layer.close(index);
                            window.parent.location.reload(); //刷新父页面
                        }, 1 * 1000);
                    }else {
                        layer.msg(result);
                    };
                },
                error: function () {
                    layer.msg('后台异常！未添加成功');
                }
            });
            //阻止页面跳转
            return false;
        });
    })
</script>
</body>
</html>
