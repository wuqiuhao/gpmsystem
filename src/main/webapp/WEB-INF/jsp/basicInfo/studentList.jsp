<%--
  Created by IntelliJ IDEA.
  User: lenovo
  Date: 2020/12/15
  Time: 21:53
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8" %>
<html>
<head>
    <title>学生列表</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/lib/layui-v2.5.5/css/layui.css">
    <script src="${pageContext.request.contextPath}/webjars/jquery/3.4.1/jquery.min.js"></script>
    <!-- 注意：如果你直接复制所有代码到本地，上述css路径需要改成你本地的 -->
</head>
<body>
<blockquote class="layui-elem-quote news_search">
    <div class="layui-inline">
        <div class="layui-input-inline">
            <input type="text"  id="searchInfo" name="searchInfo" value="" placeholder="请输入学生姓名" class="layui-input search_input">
        </div>
        <button class="layui-btn" data-type="reload" id="reload">搜索</button>
    </div>
    <div class="layui-inline">
        <button type="button" class="layui-btn layui-btn-normal" id="addStudent">添加学生</button>
    </div>
</blockquote>
<table class="layui-hide" id="studentInfo" lay-filter="studentInfo"></table>
<script type="text/html" id="zizeng">
    {{d.LAY_TABLE_INDEX+1}}
</script>
<script type="text/html" id="barDemo">
    <a class="layui-btn layui-btn-xs" lay-event="detail">查看</a>
    <a class="layui-btn layui-btn-xs" lay-event="edit">编辑</a>
    <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删除</a>
</script>
<script src="${pageContext.request.contextPath}/static/lib/layui-v2.5.5/layui.js" charset="utf-8"></script>
<!-- 注意：如果你直接复制所有代码到本地，上述js路径需要改成你本地的 -->
<script>
    layui.use('table', function() {
        var table = layui.table
           , layer = layui.layer
           , $ = layui.jquery;
        table.render({
            elem: '#studentInfo'
            , id: 'studentTable'
            , url: '${pageContext.request.contextPath}/getAllStudents'//接收后端数据
            , toolbar: '#toolbarDemo' //开启头部工具栏，并为其绑定左侧模板
            , title: '教师信息表'
            , cols: [[
                {type: 'checkbox', fixed: 'left'}
                ,{field:'zizeng', width:80, title: '序号',fixed: 'left',templet:'#zizeng'}
                , {field: 'id', title: 'ID', width: 100, hide:true}
                , {field: 'studentNo', title: '学号', width: 120, edit: 'text'}
                , {field: 'studentName', title: '姓名', width: 88}
                , {field: 'gradeId', title: '学届ID', width: 80, edit: 'text',hide:true}
                , {field: 'grade', title: '年级', width: 80, edit: 'text'}
                , {field: 'degree', title: '学制', width: 80, edit: 'text',templet:function (d) {
                        if (d.degree == 0) return '专科'
                        else return '本科'
                    }}
                , {field: 'sex', title: '性别', width: 80, edit: 'text',templet:function (d) {
                        if (d.sex == 0) return '男'
                        else return '女'
                    }}
                , {field: 'phone', title: '联系电话', width: 110}
                , {field: 'email', title: '邮箱', width: 150, edit: 'text'}
                , {field: 'qQ', title: 'QQ', width: 120}
                , {field: 'majorId', title: '专业ID', width: 100,hide:true}
                , {field: 'majorName', title: '专业', width: 100}
                , {field: 'teacherId', title: '指导老师ID', width: 100,hide:true}
                , {field: 'teacherName', title: '指导老师', width: 100}
                , {title: '操作', toolbar: '#barDemo', width: 188}
            ]]
            , page: true
        });
        //监听添加
        $("#addStudent").click(function(){
            layer.open({
                type: 2,
                skin: 'layui-layer-rim',
                area: ['70%', '80%'],
                content: '${pageContext.request.contextPath}/addStudent' //调到新增页面
            })
        });
        //监听查询
        var $=layui.$,active={
            reload:function(){
                //执行重载
                table.reload('studentTable',{
                    method:'post',
                    where:{
                        studentName:$('#searchInfo').val(),
                    }
                });
            }
        };
        $('#reload').on('click', function(){
            var type = $(this).data('type');
            active[type] ? active[type].call(this) : '';
        });
        //监听行工具事件
        table.on('tool(studentInfo)', function(obj){
            var data = obj.data;
            s_id = data.id;
            studentNo = data.studentNo;
            studentName = data.studentName;
            sex = data.sex;
            phone = data.phone;
            email = data.email;
            qQ = data.qQ;
            gradeId = data.gradeId;
            degree = data.degree;
            majorId = data.majorId;
            teacherId = data.teacherId;
            if(obj.event === 'del'){
                layer.confirm('真的删除行么', function(index){
                    $.ajax({
                        type: "POST",
                        dataType: "text",
                        url: "${pageContext.request.contextPath}/delStudent",
                        data:{"id":data.id},
                        success: function (result) {
                            result = JSON.parse(result);
                            if (result === "1") {
                                layer.msg('删除成功，1秒后自动关闭该窗口');
                                //延迟1秒执行，目的是让用户看到提示
                                setTimeout(function () {
                                    //1、先得到当前iframe层（弹出层）的索引  ///2、提交成功关闭弹出层窗口
                                    const index = parent.layer.getFrameIndex(window.name);
                                    parent.layer.close(index);
                                    window.parent.location.reload(); //刷新父页面
                                }, 1 * 1000);
                            }else {
                                layer.msg(result);
                            };
                        },
                        error: function () {
                            layer.msg('后台异常！未删除成功');
                        }
                    });
                    layer.close(index);
                });
            } else if (obj.event === 'edit') {
                layer.open({
                    type: 2,
                    skin: 'layui-layer-rim',
                    area: ['70%', '80%'],
                    content: '${pageContext.request.contextPath}/editStudent' //调到编辑页面
                })
            } else if (obj.event === 'detail') {
                layer.open({
                    type: 2,
                    skin: 'layui-layer-rim',
                    area: ['70%', '80%'],
                    content: '${pageContext.request.contextPath}/studentDetail' //调到详情页面
                })
            }
        });
    })
</script>
</body>
</html>
