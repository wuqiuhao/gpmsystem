<%--
  Created by IntelliJ IDEA.
  User: lenovo
  Date: 2020/12/30
  Time: 22:52
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>学届信息</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/lib/layui-v2.5.5/css/layui.css">
    <script src="${pageContext.request.contextPath}/webjars/jquery/3.4.1/jquery.min.js"></script>
    <!-- 注意：如果你直接复制所有代码到本地，上述css路径需要改成你本地的 -->
</head>
<body>
<blockquote class="layui-elem-quote news_search">
    <div class="layui-inline">
        <div class="layui-input-inline">
            <input type="text"  id="searchInfo" name="searchInfo" value="" placeholder="请输入学届" class="layui-input search_input">
        </div>
        <button class="layui-btn" data-type="reload" id="reload">搜索</button>
    </div>
    <div class="layui-inline">
        <button type="button" class="layui-btn" id="addAcademic">添加学届</button>
    </div>
</blockquote>
<table class="layui-hide" id="academicInfo" lay-filter="academicInfo"></table>
<script type="text/html" id="zizeng">
    {{d.LAY_TABLE_INDEX+1}}
</script>
<script type="text/html" id="barDemo">
    <a class="layui-btn layui-btn-xs" lay-event="detail">查看</a>
    <a class="layui-btn layui-btn-xs" lay-event="edit">编辑</a>
    <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删除</a>
</script>
<script src="${pageContext.request.contextPath}/static/lib/layui-v2.5.5/layui.js" charset="utf-8"></script>
<!-- 注意：如果你直接复制所有代码到本地，上述js路径需要改成你本地的 -->
<script>
    layui.use('table', function () {
        var table = layui.table
            , layer = layui.layer
            , $ = layui.jquery;
        //监听添加
        $("#addAcademic").click(function(){
            layer.open({
                type: 2,
                skin: 'layui-layer-rim',
                area: ['40%', '40%'],
                content: '${pageContext.request.contextPath}/addAcademic' //调到新增页面
            })
        });
        //获取学届表信息
        table.render({
            elem: '#academicInfo'
            , id: 'academicTable'
            , url: '${pageContext.request.contextPath}/getGradeInfo'//接收后端数据
            , toolbar: '#toolbarDemo' //开启头部工具栏，并为其绑定左侧模板
            , title: '学届信息表'
            , cols: [[
                {type: 'checkbox', fixed: 'left'}
                ,{field:'zizeng', width:80, title: '序号',fixed: 'left',templet:'#zizeng'}
                , {field: 'id', title: 'ID', width: 100, hide:true}
                , {field: 'grade', title: '学届', width: 580, edit: 'text'}
                , {title: '操作', toolbar: '#barDemo', width: 400}
            ]]
            , page: true
        });
        //监听查询
        var $=layui.$,active={
            reload:function(){
                //执行重载
                table.reload('academicTable',{
                    method:'post',
                    where:{
                        grade:$('#searchInfo').val(),
                    }
                });
            }
        };
        $('#reload').on('click', function(){
            var type = $(this).data('type');
            active[type] ? active[type].call(this) : '';
        });
        //监听行工具事件
        //表格 删除 编辑 查看操作
        table.on('tool(academicInfo)', function (obj) {
            var data = obj.data;
            id = data.id;
            grade = data.grade;
            if (obj.event === 'del') {
                layer.confirm('真的删除行么', function (index) {
                    $.ajax({
                        type: "POST",
                        dataType: "text",
                        url: "${pageContext.request.contextPath}/delGrade",
                        data: {"id": data.id},
                        success: function (result) {
                            result = JSON.parse(result);
                            if (result === "1") {
                                layer.msg('删除成功，1秒后自动关闭该窗口');
                                //延迟1秒执行，目的是让用户看到提示
                                setTimeout(function () {
                                    //1、先得到当前iframe层（弹出层）的索引  ///2、提交成功关闭弹出层窗口
                                    const index = parent.layer.getFrameIndex(window.name);
                                    parent.layer.close(index);
                                    window.parent.location.reload(); //刷新父页面
                                }, 1 * 1000);
                            } else {
                                layer.msg(result);
                            }
                            ;
                        },
                        error: function () {
                            layer.msg('后台异常！未删除成功');
                        }
                    });
                    layer.close(index);
                });
            } else if (obj.event === 'edit') {
                layer.open({
                    type: 2,
                    skin: 'layui-layer-rim',
                    area: ['40%', '40%'],
                    content: '${pageContext.request.contextPath}/academicEdit' //调到新增页面
                })
            } else if (obj.event === 'detail') {
                layer.open({
                    type: 2,
                    skin: 'layui-layer-rim',
                    btn:['取消'],
                    area: ['40%', '40%'],
                    content: '${pageContext.request.contextPath}/academicDetail' //调到详情页面
                })
            }
        });
    })
</script>
</body>
</html>
