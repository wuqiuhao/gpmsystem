<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%--
  Created by IntelliJ IDEA.
  User: lenovo
  Date: 2021/3/9
  Time: 21:20
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>论文信息</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/lib/layui-v2.5.5/css/layui.css">
    <script src="${pageContext.request.contextPath}/webjars/jquery/3.4.1/jquery.min.js"></script>
    <!-- 注意：如果你直接复制所有代码到本地，上述css路径需要改成你本地的 -->
</head>
<body>
<blockquote class="layui-elem-quote news_search">
    <div class="layui-inline">
        <div class="layui-input-inline">
            <input type="text"  id="searchInfo" name="searchInfo" value="" placeholder="请输入学生姓名" class="layui-input search_input">
        </div>
        <button class="layui-btn" data-type="reload" id="reload">搜索</button>
    </div>
</blockquote>
<table class="layui-hide" id="PaperInfo" lay-filter="PaperInfo"></table>
<script type="text/html" id="zizeng">
    {{d.LAY_TABLE_INDEX+1}}
</script>
<script type="text/html" id="barDemo">
    {{#  if(${sessionScope.permission}== "2"){ }}
    <a class="layui-btn layui-btn-xs" lay-event="detail">查看或批注</a>
    {{#  } }}
    {{#  if(${sessionScope.permission}== "1"){ }}
    <a class="layui-btn layui-btn-xs" lay-event="detail">查看</a>
    {{#  } }}
</script>
<script src="${pageContext.request.contextPath}/static/lib/layui-v2.5.5/layui.js" charset="utf-8"></script>
<!-- 注意：如果你直接复制所有代码到本地，上述js路径需要改成你本地的 -->
<script>
    layui.use('table', function() {
        var table = layui.table;
        layer = layui.layer,
            $ = layui.jquery;
        table.render({
            elem: '#PaperInfo'
            , id : 'PaperInfoTable'
            , url: '${pageContext.request.contextPath}/getPaperInfo'//接收后端数据
            , toolbar: '#toolbarDemo' //开启头部工具栏，并为其绑定左侧模板
            , title: '开题报告信息表'
            , cols: [[
                {type: 'checkbox', fixed: 'left'}
                ,{field:'zizeng', width:80, title: '序号',fixed: 'left',templet:'#zizeng'}
                , {field: 'id', title: 'ID', width: 100, hide:true}
                , {field: 'studentName', title: '学生姓名', width: 160}
                , {field: 'paperName', title: '论文名称', width: 180}
                , {field: 'teacherName', title: '教师姓名', width: 180}
                , {field: 'paperdescription', title: '描述', width: 180}
                , {field: 'version', title: '版本', width: 180}
                , {field: 'paperaddress', title: '地址', width: 180,hide:true}
                , {title: '操作', toolbar: '#barDemo', width: 300}
            ]]
            , page: true
        });
        /*//监听添加
        $("#upload").click(function(){
            layer.open({
                type: 2,
                skin: 'layui-layer-rim',
                area: ['60%', '70%'],
                content: '${pageContext.request.contextPath}/upload' //调到新增页面
            })
        });*/
        //监听查询
        var $=layui.$,active={
            reload:function(){
                //执行重载
                table.reload('PaperInfoTable',{
                    method:'post',
                    where:{
                        studentName:$('#searchInfo').val(),
                    }
                });
            }
        };
        $('#reload').on('click', function(){
            var type = $(this).data('type');
            active[type] ? active[type].call(this) : '';
        });
        //监听行工具事件
        table.on('tool(PaperInfo)', function(obj){
            var data = obj.data;
            departmentName = data.departmentName;
            departmentDesc = data.description;
            if(obj.event === 'detail'){
                var url = "${pageContext.request.contextPath}/wordView";	//请求路径
                var type = "POST";	//请求类型
                var jsonData = [];	//构建json
                jsonData.push({"path":data.paperaddress});	//将传参放入json中
                var targetType = "_self";	//在当前页面打开
                formSubmit(url,type,jsonData,targetType)
            } else if (obj.event === 'edit') {
                var url = "${pageContext.request.contextPath}/wordView";	//请求路径
                var type = "POST";	//请求类型
                var jsonData = [];	//构建json
                jsonData.push({"path":data.paperaddress});	//将传参放入json中
                var targetType = "_self";	//在当前页面打开
                formSubmit(url,type,jsonData,targetType)
            }
        });
    })
    function formSubmit(url,type,jsonData,targetType) {
        var dlform = document.createElement('form');
        dlform.style = "display:none;";
        dlform.method = type;
        dlform.action = url;
        dlform.target = targetType
        var json = jsonData;
        for(var i=0,l=json.length;i<l;i++){
            for(var key in json[i]){
                var hdnFilePath = document.createElement('input');
                hdnFilePath.type = 'hidden';
                hdnFilePath.name = key;
                hdnFilePath.value = json[i][key];
            }
        }
        dlform.appendChild(hdnFilePath);
        document.body.appendChild(dlform);
        dlform.submit();
        document.body.removeChild(dlform);
    }
</script>
</body>
</html>
