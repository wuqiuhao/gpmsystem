<%--
  Created by IntelliJ IDEA.
  User: lenovo
  Date: 2021/1/11
  Time: 9:10
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>教师主页</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/lib/layui-v2.5.5/css/layui.css">
    <script src="${pageContext.request.contextPath}/webjars/jquery/3.4.1/jquery.min.js"></script>
    <!-- 注意：如果你直接复制所有代码到本地，上述css路径需要改成你本地的 -->
</head>
<style type="text/css">
    .container {
        width: 100%;
        height: 100%;
    }

    #welcome {
        width: 100%;
        height: 100%;
    }

    body {
        overflow: hidden;
    }
</style>
<body>
<ul class="layui-nav">
    <li class="layui-nav-item">
        <h2>毕业设计管理系统（教师端）</h2>
    </li>
    <li class="layui-nav-item">
        <span lay-separator=""><i class="layui-icon">&#xe623;</i></span>
    </li>
    <li class="layui-nav-item">
        <a href="${pageContext.request.contextPath}/myStudentInfo" target="iframe">我的学生<span
                class="layui-badge-dot"></span></a>
    </li>
    <li class="layui-nav-item">
        <a href="${pageContext.request.contextPath}/topicInfo" target="iframe">课题<span
                class="layui-badge-dot"></span></a>
    </li>
    <li class="layui-nav-item">
        <a href="${pageContext.request.contextPath}/reportInfo?page=1&limit=10" target="iframe">开题报告<span
                class="layui-badge-dot"></span></a>
    </li>
    <li class="layui-nav-item">
        <a href="${pageContext.request.contextPath}/paperInfo" target="iframe">论文<span
                class="layui-badge-dot"></span></a>
    </li>
    <li class="layui-nav-item" lay-unselect="" style="float: right">
        <a href="javascript:">当前用户：${sessionScope.userNo}</a>
        <dl class="layui-nav-child">
            <dd><a href="${pageContext.request.contextPath}/logout">退出</a></dd>
        </dl>
    </li>
</ul>
<div class="container">
    <iframe src="${pageContext.request.contextPath}/welcome-t" frameborder="0" id="welcome"
            name="iframe"></iframe>
</div>
<script src="${pageContext.request.contextPath}/static/lib/layui-v2.5.5/layui.js" charset="utf-8"></script>
<!-- 注意：如果你直接复制所有代码到本地，上述js路径需要改成你本地的 -->
<script>
    layui.use('element', function () {
        var element = layui.element; //导航的hover效果、二级菜单等功能，需要依赖element模块

        //监听导航点击
        element.on('nav(demo)', function (elem) {
            //console.log(elem)
            layer.msg(elem.text());
        });
    });
</script>
</body>
</html>
