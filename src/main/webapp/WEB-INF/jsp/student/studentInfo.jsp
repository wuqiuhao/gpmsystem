<%--
  Created by IntelliJ IDEA.
  User: lenovo
  Date: 2020/12/15
  Time: 21:53
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8" %>
<html>
<head>
    <meta charset="utf-8">
    <title>个人信息</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/lib/layui-v2.5.5/css/layui.css">
    <script src="${pageContext.request.contextPath}/webjars/jquery/3.4.1/jquery.min.js"></script>
    <!-- 注意：如果你直接复制所有代码到本地，上述css路径需要改成你本地的 -->
    <style type="text/css">
        body{
            margin-left: 10px;
        }
    </style>
</head>
<body>
<form class="layui-form layui-form-pane" id="studentInfo">
    <div class="layui-form-item">
        <label class="layui-form-label">学号</label>
        <div class="layui-input-block">
            <input type="text" id="studentNo" name="studentNo" autocomplete="off" class="layui-input" readonly=readonly>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">姓名</label>
        <div class="layui-input-block">
            <input type="text" id="studentName" name="studentName" autocomplete="off" class="layui-input" readonly=readonly>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">专业</label>
        <div class="layui-input-block">
            <select id="majorId" name="majorId" lay-filter="aihao" disabled="disabled">
                <option value=""></option>
            </select>
        </div>
    </div>
    <div class="layui-form-item" pane="">
        <label class="layui-form-label">性别</label>
        <div class="layui-input-block">
            <input type="radio" id="sex0" name="sex" value="0" title="男">
            <input type="radio" id="sex1" name="sex" value="1" title="女">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">联系电话</label>
        <div class="layui-input-block">
            <input type="text" id="phone" name="phone" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">邮箱</label>
        <div class="layui-input-block">
            <input type="text" id="email" name="email" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">QQ</label>
        <div class="layui-input-block">
            <input type="text" id="QQ" name="QQ" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item" style="text-align: center">
        <button class="layui-btn" lay-submit="" lay-filter="editInfo">保存修改</button>
    </div>
</form>
<script src="${pageContext.request.contextPath}/static/lib/layui-v2.5.5/layui.js" charset="utf-8"></script>
<!-- 注意：如果你直接复制所有代码到本地，上述js路径需要改成你本地的 -->
<script>

    layui.use(['form', 'upload', 'layer'],function(){
        var form = layui.form;
        $('#studentNo').val(${studentInfo.studentNo});
        $('#studentName').val("${studentInfo.studentName}");
        $('#phone').val(${studentInfo.phone});
        $('#email').val('${studentInfo.email}');
        $('#QQ').val(${studentInfo.QQ});
        var majorId = ${studentInfo.majorId};
        var sex = ${studentInfo.sex}
        $("input[name='sex'][value="+sex+"]").prop("checked",true);
        form.render();
        $.ajax({
            url: '${pageContext.request.contextPath}/majorInfo',
            dataType: 'json',
            type: 'get',
            success: function (data) {
                //使用循环遍历，给下拉列表赋值
                $.each(data.data, function (index, value) {
                    $('#majorId').append(new Option(value.majorName,value.id));// 下拉菜单里添加元素
                    $('#majorId').find('option').each(function() {
                        $(this).attr('selected', $(this).val() == majorId);
                    });
                });
                layui.form.render("select");//重新渲染 固定写法
            }
        })
        form.render();//重新渲染 固定写法
        //监听submit提交按钮 button ，lay-filter 为 proAdd 的
        form.on('submit(editInfo)', function (data) {
            $.ajax({
                type: "POST",
                dataType: "text",
                url: "${pageContext.request.contextPath}/editStudentInfo",
                data: $('#studentInfo').serialize(),  //表单数据
                success: function (result) {
                    result = JSON.parse(result);
                    if (result === "1") {
                        layer.msg('修改成功，1秒后自动关闭该窗口');
                        //延迟1秒执行，目的是让用户看到提示
                        setTimeout(function () {
                            top.location.href = '${pageContext.request.contextPath}/index';
                        }, 1 * 1000);
                    }else {
                        layer.msg(result);
                    };
                },
                error: function () {
                    layer.msg('后台异常！未修改成功');
                }
            });
            //阻止页面跳转
            return false;
        });
    });
</script>
</body>
</html>
