<%--
  Created by IntelliJ IDEA.
  User: lenovo
  Date: 2021/2/26
  Time: 11:13
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>编辑学生信息</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/lib/layui-v2.5.5/css/layui.css">
    <script src="${pageContext.request.contextPath}/webjars/jquery/3.4.1/jquery.min.js"></script>
    <!-- 注意：如果你直接复制所有代码到本地，上述css路径需要改成你本地的 -->
</head>
<body>
<div class="site-text" style="margin: 5%;">
    <form class="layui-form" id="updateStudent" lay-filter="updateStudent">
        <div class="layui-form-item" style="display: none">
            <label class="layui-form-label">ID</label>
            <div class="layui-input-block">
                <input type="text" id="id" name="id" lay-verify="title" autocomplete="off"
                       class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">学号</label>
            <div class="layui-input-block">
                <input type="text" id="studentNo" name="studentNo" lay-verify="title" autocomplete="off"
                       placeholder="请输入标题" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">姓名</label>
            <div class="layui-input-block">
                <input type="text" id="studentName" name="studentName" lay-verify="title" autocomplete="off"
                       placeholder="请输入标题" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item" class="layui-form">
            <label class="layui-form-label">学届</label>
            <div class="layui-input-block">
                <select id="gradeId" name="gradeId" lay-filter="gradeId">
                    <option value=""></option>
                </select>
            </div>
        </div>
        <div class="layui-form-item" class="layui-form">
            <label class="layui-form-label">学制</label>
            <div class="layui-input-block">
                <select id="degree" name="degree" lay-filter="degree">
                    <option value="" selected="">请选择</option>
                    <option value="0">专科</option>
                    <option value="1">本科</option>
                </select>
            </div>
        </div>
        <div class="layui-form-item" class="layui-form">
            <label class="layui-form-label">性别</label>
            <div class="layui-input-block">
                <select id="sex" name="sex" lay-filter="sex">
                    <option value="" selected="">请选择</option>
                    <option value="0">男</option>
                    <option value="1">女</option>
                </select>
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">联系电话</label>
            <div class="layui-input-block">
                <input type="text" id="phone" name="phone" lay-verify="title" autocomplete="off"
                       placeholder="请输入标题" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">邮箱</label>
            <div class="layui-input-block">
                <input type="text" id="email" name="email" lay-verify="title" autocomplete="off"
                       placeholder="请输入标题" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">QQ</label>
            <div class="layui-input-block">
                <input type="text" id="qQ" name="qQ" lay-verify="title" autocomplete="off"
                       placeholder="请输入标题" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item" class="layui-form">
            <label class="layui-form-label">专业</label>
            <div class="layui-input-block">
                <select id="majorId" name="majorId" lay-filter="majorId">
                    <option value=""></option>
                </select>
            </div>
        </div>
        <div class="layui-form-item" class="layui-form">
            <label class="layui-form-label">指导老师</label>
            <div class="layui-input-block">
                <select id="teacherId" name="teacherId" lay-filter="teacherId">
                    <option value=""></option>
                </select>
            </div>
        </div>
        <div class="layui-form-item" style="margin-top:40px">
            <div class="layui-input-block">
                <button lay-submit="" id="EditStudent" class="layui-btn layui-btn-normal layui-btn-radius">修改学生</button>
                <button type="reset" class="layui-btn layui-btn-danger layui-btn-radius">取消</button>
            </div>
        </div>
    </form>
</div>
<script src="${pageContext.request.contextPath}/static/lib/layui-v2.5.5/layui.js" charset="utf-8"></script>
<!-- 注意：如果你直接复制所有代码到本地，上述js路径需要改成你本地的 -->
<script>
    layui.use(['form'], function() {
        const form = layui.form
            , layer = layui.layer
            , $ = layui.jquery;
        form.val("editStudent", {
            "id" : parent.s_id,
            "studentNo": parent.studentNo,
            "studentName": parent.studentName,
            "gradeId": parent.gradeId,
            "degree": parent.degree,
            "sex": parent.sex,
            "phone": parent.phone,
            "email": parent.email,
            "qQ": parent.qQ,
            "majorId": parent.majorId,
            "teacherId": parent.teacherId,
        })
        var gradeId = parent.gradeId;
        var majorId = parent.majorId;
        var teacherId = parent.teacherId;
        //获取学届列表
        $.ajax({
            url: '${pageContext.request.contextPath}/getGradeList',//获取专业列表信息
            dataType: 'json',
            type: 'get',
            success: function (data) {
                //使用循环遍历，给下拉列表赋值
                $.each(data.data, function (index, value) {
                    $('#gradeId').append(new Option(value.grade,value.id));// 下拉菜单里添加元素
                    $('#gradeId').find('option').each(function() {
                        $(this).attr('selected', $(this).val() == gradeId);
                    });
                });
                layui.form.render("select");//重新渲染 固定写法
            }
        })
        //获取专业列表
        $.ajax({
            url: '${pageContext.request.contextPath}/getMajorList',//获取专业列表信息
            dataType: 'json',
            type: 'get',
            success: function (data) {
                //使用循环遍历，给下拉列表赋值
                $.each(data.data, function (index, value) {
                    $('#majorId').append(new Option(value.majorName,value.id));// 下拉菜单里添加元素
                    $('#majorId').find('option').each(function() {
                        $(this).attr('selected', $(this).val() == majorId);
                    });
                });
                layui.form.render("select");//重新渲染 固定写法
            }
        })
        //获取教师列表
        $.ajax({
            url: '${pageContext.request.contextPath}/getTeacherList',//获取教师列表信息
            dataType: 'json',
            type: 'get',
            success: function (data) {
                //使用循环遍历，给下拉列表赋值
                $.each(data.data, function (index, value) {
                    $('#teacherId').append(new Option(value.teacherName,value.id));// 下拉菜单里添加元素
                    $('#teacherId').find('option').each(function() {
                        $(this).attr('selected', $(this).val() == teacherId);
                    });
                });
                layui.form.render("select");//重新渲染 固定写法
            }
        })
        //监听submit提交按钮 button ，lay-filter 为 noticeAdd 的
        $('#EditStudent').on('click',function (){
            var params = decodeURIComponent($("#updateStudent").serialize())
            $.ajax({
                type: "POST",
                dataType: "text",
                url: "${pageContext.request.contextPath}/updateStudent",
                data: params,  //表单数据
                success: function (result) {
                    result = JSON.parse(result);
                    if (result === "1") {
                        layer.msg('修改成功，1秒后自动关闭该窗口');
                        //延迟1秒执行，目的是让用户看到提示
                        setTimeout(function () {
                            //1、先得到当前iframe层（弹出层）的索引  ///2、提交成功关闭弹出层窗口
                            const index = parent.layer.getFrameIndex(window.name);
                            parent.layer.close(index);
                            window.parent.location.reload(); //刷新父页面
                        }, 1 * 1000);
                    }else {
                        layer.msg(result);
                    };
                },
                error: function () {
                    layer.msg('后台异常！未修改成功');
                }
            });
            //阻止页面跳转
            return false;
        });
    })
</script>
</body>
</html>
