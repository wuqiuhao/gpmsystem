<%--
  Created by IntelliJ IDEA.
  User: lenovo
  Date: 2021/3/16
  Time: 15:18
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>开题报告打分</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/lib/layui-v2.5.5/css/layui.css">
    <script src="${pageContext.request.contextPath}/webjars/jquery/3.4.1/jquery.min.js"></script>
    <!-- 注意：如果你直接复制所有代码到本地，上述css路径需要改成你本地的 -->
</head>
<body>
<div class="site-text" style="margin: 5%;">
    <form class="layui-form" id="scoringR" lay-filter="scoringR">
        <div class="layui-form-item" style="display: none">
            <label class="layui-form-label">ID</label>
            <div class="layui-input-block">
                <input type="text" id="id" name="id" lay-verify="title" autocomplete="off" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">课题名称</label>
            <div class="layui-input-block">
                <input type="text" id="thesisName" name="thesisName" lay-verify="title" autocomplete="off"
                       class="layui-input" disabled>
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">学生名字</label>
            <div class="layui-input-block">
                <input type="text" id="studentName" name="studentName" lay-verify="title" autocomplete="off"
                       class="layui-input" disabled>
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">分数</label>
            <div class="layui-input-block">
                <input type="text" id="studentScore" name="studentScore" lay-verify="title" autocomplete="off"
                       placeholder="请输入1-100之间的分数" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">打分人</label>
            <div class="layui-input-block">
                <input type="text" id="rater" name="rater" lay-verify="title" autocomplete="off"
                       placeholder="请输入你的名字" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item" style="margin-top:40px">
            <div class="layui-input-block">
                <button lay-submit="" id="submitR" class="layui-btn layui-btn-normal layui-btn-radius">点击确认</button>
            </div>
        </div>
    </form>
</div>
<script src="${pageContext.request.contextPath}/static/lib/layui-v2.5.5/layui.js" charset="utf-8"></script>
<!-- 注意：如果你直接复制所有代码到本地，上述js路径需要改成你本地的 -->
<script>
    layui.use(['form','upload'], function() {
        const form = layui.form
            , layer = layui.layer
            ,upload = layui.upload
            , $ = layui.jquery;
        form.val("scoringR", {
            "id":parent.id,
            "thesisName":parent.thesisName,
            "studentName":parent.studentName,
        })
        //监听submit提交按钮 button ，lay-filter 为 noticeAdd 的
        $('#submitR').on('click',function (){
            $.ajax({
                type: "POST",
                dataType: "text",
                url: "${pageContext.request.contextPath}/scoring",
                data: $('#scoringR').serialize(),  //表单数据
                success: function (result) {
                    result = JSON.parse(result);
                    if (result === "1") {
                        layer.msg('打分成功，1秒后自动关闭该窗口');
                        //延迟1秒执行，目的是让用户看到提示
                        setTimeout(function () {
                            //1、先得到当前iframe层（弹出层）的索引  ///2、提交成功关闭弹出层窗口
                            const index = parent.layer.getFrameIndex(window.name);
                            parent.layer.close(index);
                            window.parent.location.reload(); //刷新父页面
                        }, 1 * 1000);
                    }else {
                        layer.msg(result);
                    };
                },
                error: function () {
                    layer.msg('后台异常！未打分成功');
                }
            });
            //阻止页面跳转
            return false;
        });
    })
</script>
</body>
</html>
