<%--
  Created by IntelliJ IDEA.
  User: lenovo
  Date: 2021/2/24
  Time: 12:28
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>编辑教师信息</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/lib/layui-v2.5.5/css/layui.css">
    <script src="${pageContext.request.contextPath}/webjars/jquery/3.4.1/jquery.min.js"></script>
    <!-- 注意：如果你直接复制所有代码到本地，上述css路径需要改成你本地的 -->
</head>
<body>
<div class="site-text" style="margin: 5%;">
    <form class="layui-form" id="editTeacher" lay-filter="editTeacher">
        <div class="layui-form-item" style="display: none">
            <label class="layui-form-label">ID</label>
            <div class="layui-input-block">
                <input type="text" id="id" name="id" lay-verify="title" autocomplete="off"
                       class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">教师工号</label>
            <div class="layui-input-block">
                <input type="text" id="teacherNo" name="teacherNo" lay-verify="title" autocomplete="off"
                       placeholder="请输入标题" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">姓名</label>
            <div class="layui-input-block">
                <input type="text" id="teacherName" name="teacherName" lay-verify="title" autocomplete="off"
                       placeholder="请输入标题" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item" class="layui-form">
            <label class="layui-form-label">性别</label>
            <div class="layui-input-block">
                <select id="sex" name="sex" lay-filter="teacherList">
                    <option value="0">男</option>
                    <option value="1">女</option>
                </select>
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">联系电话</label>
            <div class="layui-input-block">
                <input type="text" id="phone" name="phone" lay-verify="title" autocomplete="off"
                       placeholder="请输入标题" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">邮箱</label>
            <div class="layui-input-block">
                <input type="text" id="email" name="email" lay-verify="title" autocomplete="off"
                       placeholder="请输入标题" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">QQ</label>
            <div class="layui-input-block">
                <input type="text" id="qQ" name="qQ" lay-verify="title" autocomplete="off"
                       placeholder="请输入标题" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item" class="layui-form">
            <label class="layui-form-label">所属学院</label>
            <div class="layui-input-block">
                <select id="departmentId" name="departmentId" lay-filter="departmentId">
                    <option value=""></option>
                </select>
            </div>
        </div>
        <div class="layui-form-item" style="margin-top:40px">
            <div class="layui-input-block">
                <button lay-submit="" id="updateTeacher" class="layui-btn layui-btn-normal layui-btn-radius">点击修改</button>
            </div>
        </div>
    </form>
</div>
<script src="${pageContext.request.contextPath}/static/lib/layui-v2.5.5/layui.js" charset="utf-8"></script>
<!-- 注意：如果你直接复制所有代码到本地，上述js路径需要改成你本地的 -->
<script>
    layui.use(['form'], function() {
        const form = layui.form
            , layer = layui.layer;
        form.val("editTeacher", {
            "id" : parent.id,
            "teacherNo": parent.teacherNo,
            "teacherName": parent.teacherName,
            "sex": parent.sex,
            "phone": parent.phone,
            "email": parent.email,
            "qQ": parent.qQ,
            "departmentId": parent.departmentId,
        })
        var departmentID = parent.departmentId;
        //学院选择
        $.ajax({
            url: '${pageContext.request.contextPath}/getDpmList',//获取学院列表信息
            dataType: 'json',
            type: 'get',
            success: function (data) {
                //使用循环遍历，给下拉列表赋值
                $.each(data.data, function (index, value) {
                    $('#departmentId').append(new Option(value.departmentName,value.id));// 下拉菜单里添加元素
                    $('#departmentId').find('option').each(function() {
                        $(this).attr('selected', $(this).val() == departmentID);
                    });
                });
                layui.form.render("select");//重新渲染 固定写法
            }
        })
        //监听submit提交按钮 button ，lay-filter 为 noticeAdd 的
        $('#updateTeacher').on('click',function (){
            var params = decodeURIComponent($("#editTeacher").serialize())
            $.ajax({
                type: "POST",
                dataType: "text",
                url: "${pageContext.request.contextPath}/updateTeacher",
                data: params,  //表单数据
                success: function (result) {
                    result = JSON.parse(result);
                    if (result === "1") {
                        layer.msg('修改成功，1秒后自动关闭该窗口');
                        //延迟1秒执行，目的是让用户看到提示
                        setTimeout(function () {
                            //1、先得到当前iframe层（弹出层）的索引  ///2、提交成功关闭弹出层窗口
                            const index = parent.layer.getFrameIndex(window.name);
                            parent.layer.close(index);
                            window.parent.location.reload(); //刷新父页面
                        }, 1 * 1000);
                    }else {
                        layer.msg(result);
                    };
                },
                error: function () {
                    layer.msg('后台异常！未修改成功');
                }
            });
            //阻止页面跳转
            return false;
        });
    })
</script>
</body>
</html>
