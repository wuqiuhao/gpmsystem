function rightClick() {
    let e = window.event || event;
    e.preventDefault();
    let selectObj = window.getSelection();
    let selectText = window.getSelection().toString();

    if (selectText !== '' || selectText.length > 0) {
        let menuElement = document.getElementById('menu');
        menuElement.style.display = 'block';

        let x = 0, y = 0;
        let doc = document.documentElement;
        let body = document.body;
        if(!event) event=window.event;
        if (window.pageYOffset) {
            x = window.pageXOffset;
            y = window.pageYOffset;
        }else{
            x = (doc && doc.scrollLeft || body && body.scrollLeft || 0)
                - (doc && doc.clientLeft || body && body.clientLeft || 0);
            y = (doc && doc.scrollTop || body && body.scrollTop || 0)
                - (doc && doc.clientTop || body && body.clientTop || 0);
        }
        x += event.clientX;
        y += event.clientY;

        menuElement.style.left = x + 'px';
        menuElement.style.top = y + 'px';

        let liArrays = menuElement.getElementsByTagName('li');
        let selectRange = selectObj.getRangeAt(0);

        let firstNode = selectRange.startContainer.parentNode;
        let secondNode = selectRange.endContainer.parentNode;
        let flag = (firstNode === secondNode);

        liArrays[0].onclick = function () {
            if (!flag) {
                return;
            }
            try {
                let bolderBottom = document.createElement('u');
                bolderBottom.className = 'underline';

                selectRange.surroundContents(bolderBottom);
            } catch (error) {
                /*if ('InvalidStateError' === error.name) {
                    alert('不支持对已添加过的样式再次添加');
                }*/
            }
        };

        liArrays[1].onclick = function () {
            if (!flag) {
                return;
            }
            try {
                let array = firstNode.childNodes;
                for (let n = 0; n < array.length; n++) {
                    if (array[n].className === 'radiusSpan') {
                        return false;
                    }
                }
                let radius = document.createElement('span');
                let text = document.createElement('span');
                radius.className = 'radiusSpan';
                text.className = 'radiusText';
                let Id = String(document.getElementsByClassName('radiusSpan').length + 1);
                text.innerText = Id;
                radius.appendChild(text);

                firstNode.appendChild(radius);

                let card = document.createElement('div');
                card.className = 'card';
                let header = document.createElement('div');
                header.className = 'card-header';
                header.id = 'heading' + Id;
                let h2 =  document.createElement('h2');
                h2.className = 'mb-0';
                let btn = document.createElement('button');
                btn.className = 'btn-btn-link';
                btn.setAttribute('data-toggle', 'collapse');
                btn.setAttribute('data-target', '#collapse' + Id);
                btn.setAttribute('aria-expanded', 'true');
                btn.setAttribute('aria-controls', 'collapse' + Id);
                btn.innerText = Id;
                btn.style.height = '20px';
                btn.style.fontSize = '9px';
                h2.appendChild(btn);
                header.appendChild(h2);
                card.appendChild(header);

                let collapse = document.createElement('div');
                collapse.id = 'collapse' + Id;
                collapse.className = (Id === '1') ? 'collapse show' : 'collapse';
                collapse.setAttribute('aria-labelledby',header.id);
                collapse.setAttribute('data-parent','#note');
                let cardBody = document.createElement('div');
                cardBody.className = 'card-body';
                cardBody.style.fontSize = '12px';
                cardBody.contentEditable = 'true';
                collapse.appendChild(cardBody);
                card.appendChild(collapse);

                let note = document.getElementById('note');
                note.appendChild(card);
            } catch (error) { 

            }
        };

        liArrays[2].onclick = function () {
            if (!flag) {
                alert('不支持对不同行直接操作');
                return;
            }
            try {
                let highLight = document.createElement('font');
                highLight.className = 'highLight';
                selectRange.surroundContents(highLight);
            } catch (error) {
                if ('InvalidStateError' === error.name) {
                    alert('不支持对已添加过的样式再次添加');
                }
            }
        };

        window.onclick = function () {
            let menuElement = document.getElementById('menu');
            menuElement.style.display = 'none';
        };
    }
}