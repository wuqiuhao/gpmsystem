package dao;

import entity.Grade;
import entity.Notice;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @author wqh
 * @date 2021/1/6 21:13
 */
public interface BasicInfoDao {

    //查询所有公告信息
    public List<Notice> getAllNoticeInfo(Map<String,Object> map);
    //查询总数
    public int getNoticeCount();
    //修改公告信息
    public int updateNoticeInfo(Notice notice);
    //新增公告
    public int insertNoticeInfo(Notice notice);
    //根据ID删除公告信息
    public int delNoticeInfo(@Param("id") int id);


    //查询学届信息
    public List<Grade> findAllGrade(Map<String,Object> map);
    //学届下拉列表
    public List<Grade> getGradeList();
    //查询总数
    public int getGradeCount();
    //新增学届
    public int insertGradeInfo(Grade grade);
    //修改学届
    public int updateGradeInfo(Grade grade);
    //删除学届
    public int delGradeInfo(@Param("id") int id);
}
