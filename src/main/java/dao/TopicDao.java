package dao;

import entity.Report;
import entity.Topic;
import entity.TopicUnit;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @author wqh
 * @date 2021/3/8 16:34
 */
public interface TopicDao {
    /**
     * 查询列表
     * @return
     */
    public List<TopicUnit> getAllTopicInfo(Map<String,Object> map);
    /**
     * 查询总数
     * @return
     */
    public int getCountTopic();
    //新增课题
    public int addTopic(Topic topic);
    //修改课题
    public int updateTopic(Topic topic);
    //删除课题
    public int delTopic(@Param("id") int id);
    //当前学生课题
    public List<TopicUnit> getMyTopic(Map<String,Object> map);
    /**
     * 提交任务书
     * @param topic 任务书信息
     * @return 返回执行成功条数
     */
    int subTopic(Topic topic);

    /**
     * 查找任务书路径下载
     * @param topicid 课题id
     * @return 返回任务书路径
     */
    Topic downloadTaskBook(@Param("id") int topicid);
}
