package dao;

import entity.Report;
import entity.ReportUnit;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @author wqh
 * @date 2021/3/8 16:15
 */
public interface ReportDao {
    /**
     * 查询列表
     * @return
     */
    public List<ReportUnit> getAllReportInfo(Map<String,Object> map);
    /**
     * 查询总数
     * @return
     */
    public int getCountReport();
    //上传开题报告
    public int insertReport(Report report);
    //修改开题报告
    public int updateReport(Report report);
    //删除开题报告
    public int delReport(@Param("id") int id);
    /**
     * 提交开题报告
     * @param report 开题报告信息
     * @return 返回执行成功条数
     */
    int subReport(Report report);
}
