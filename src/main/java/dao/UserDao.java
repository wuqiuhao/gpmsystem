package dao;

import entity.Student;
import entity.Teacher;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author wqh
 * @date 2020/12/8 17:30
 */
public interface UserDao {
    /**
     * 登陆判断
     * @return
     */
    public int StudentLogin(@Param("studentNo") String studentNo,
                            @Param("studentPassword") String studentPassword);

    public int TeacherLogin(@Param("teacherNo") String teacherNo,
                            @Param("teacherPassword") String teacherPassword);
    /**
     * 查询当前用户信息
     * @return
     */
    public Student getStudentInfo(@Param("studentNo") String studentNo);

    public Teacher getTeacherInfo(@Param("teacherNo") String teacherNo);

    /**
     * 修改密码
     * @param userPasssword
     * @return
     */
    public int updateStudentPsw(@Param("studentNo") String studentNo,@Param("studentPassword") String studentPassword);

    public int updateTeacherPsw(@Param("teacherNo") String teacherNo,@Param("teacherPassword") String teacherPassword);
    /**
     * 修改电话号码
     * @param studentNo
     * @param phone
     * @return
     */
    public int updatePhone(@Param("userNo") String userNo,@Param("phone") String phone,@Param("permission") Long permission);

    /**
     * 修改邮箱
     * @param studentNo
     * @param email
     * @return
     */
    public int updateEmail(@Param("userNo") String userNo,@Param("email") String email,@Param("permission")long permission);

    /**
     * 修改QQ号码
     * @param studentNo
     * @param QQ
     * @return
     */
    public int updateQQ(@Param("userNo") String userNo,@Param("QQ") String QQ,@Param("permission")long permission);
    //查询教师ID
    public int getTeacherID(String teacherNo);
    //获取当前学生对应的教师ID
    public int getTeacherIdByNo(String studentNo);
    //获取当前学生ID
    public int getStudentID(String studentNo);
}
