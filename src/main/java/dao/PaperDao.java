package dao;

import entity.Paper;
import entity.PaperUnit;

import java.util.List;
import java.util.Map;

/**
 * @author wqh
 * @date 2021/3/8 16:46
 */
public interface PaperDao {
    /**
     * 查询列表
     * @return
     */
    public List<PaperUnit> getAllPaperInfo(Map<String,Object> map);
    /**
     * 查询总数
     * @return
     */
    public int getCountPaper();
    /**
     * 提交论文
     * @param paper 论文信息
     * @return 返回执行成功条数
     */
    int subPaper(Paper paper);
}
