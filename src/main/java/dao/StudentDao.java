package dao;

import entity.Student;
import entity.StudentUnit;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @author wqh
 * @date 2020/12/16 22:12
 */
public interface StudentDao {
    //查询学生列表
    public List<StudentUnit> findAllStudents(Map<String,Object> map);
    //查询总条数
    public int getStudentCount();
    //新增学生
    public int insertStudentInfo(Student student);
    //修改学生信息
    public int updateStudentInfo(Student student);
    //删除学生信息
    public int delStudentInfo(@Param("id")int id);
    //学生列表
    public List<Student> getStudentList(@Param("teacherId") int teacherId);
}
