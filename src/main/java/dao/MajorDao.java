package dao;

import entity.Major;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @author wqh
 * @date 2021/1/10 15:33
 */
public interface MajorDao {

    //专业信息
    public List<Major> getAllMajor(Map<String,Object> map);
    //专业下拉列表
    public List<Major> getMajorList();
    //数量
    public int getCountMajor();
    //新增专业
    public int insertMajorInfo(Major major);
    //修改专业信息
    public int updateMajorInfo(Major major);
    //删除专业信息
    public int delMajorInfo(@Param("id") int id);
}
