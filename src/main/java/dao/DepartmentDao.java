package dao;

import entity.Department;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @author wqh
 * @date 2020/12/16 19:58
 */
public interface DepartmentDao {
     //查询学院信息
    public List<Department> getAllDepartment(Map<String,Object> map);
     //查询总数
    public int getCountDepartment();
    //获取下拉列表
    public List<Department> getDpmList();
    //新增学院信息
    public int insertDepartmentInfo(Department department);
    //修改学院信息
    public int updateDepartmentInfo(Department department);
    //删除学院信息
    public int delDeparmentInfo(@Param("dpm_id")int dpm_id);
}
