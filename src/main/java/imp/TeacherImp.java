package imp;

import entity.Student;
import entity.Teacher;
import entity.TeacherUnit;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @author wqh
 * @date 2020/12/19 20:21
 */
public interface TeacherImp {
    //查询教师列表
    public List<TeacherUnit> getAllTeacherInfo(Map<String,Object> map);
    //查询教师总数
    public int getCountTeacher();
    //获取教师下拉列表
    public List<Teacher> getTeacherList();
    //新增教师信息
    public String insertTeacherInfo(Teacher teacher);
    //修改教师信息
    public String updateTeacherInfo(Teacher teacher);
    //删除教师信息
    public String delTeacherInfo(@Param("id") int id);
}
