package imp;

import entity.Department;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @author wqh
 * @date 2020/12/16 20:02
 */
public interface DepartmentImp {
    //查询学院信息
    public List<Department> getAllDepartment(Map<String,Object> map);
    //获取下拉列表
    public List<Department> getDpmList();
    //查询总数
    public int getCountDepartment();
    //新增学院信息
    public String insertDepartmentInfo(Department department);
    //修改学院信息
    public String updateDepartmentInfo(Department department);
    //删除学院信息
    public String delDeparmentInfo(int dpm_id);
}
