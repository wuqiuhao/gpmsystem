package imp;

import entity.Report;
import entity.ReportUnit;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @author wqh
 * @date 2021/3/8 16:19
 */
public interface ReportImp {
    /**
     * 查询列表
     * @return
     */
    public List<ReportUnit> getAllReportInfo(Map<String,Object> map);
    /**
     * 查询总数
     * @return
     */
    public int getCountReport();
    //上传开题报告
    public String insertReport(Report report);
    //修改开题报告
    public String updateReport(Report report);
    //删除开题报告
    public String delReport(@Param("id") int id);
    //上传开题报告
    public String subReport(Report report);
}
