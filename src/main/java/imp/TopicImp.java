package imp;

import entity.Topic;
import entity.TopicUnit;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @author wqh
 * @date 2021/3/8 16:36
 */
public interface TopicImp {
    /**
     * 查询列表
     * @return
     */
    public List<TopicUnit> getAllTopicInfo(Map<String,Object> map);
    /**
     * 查询总数
     * @return
     */
    public int getCountTopic();
    //新增课题
    public String addTopic(Topic topic);
    //修改课题
    public String updateTopic(Topic topic);
    //删除课题
    public String delTopic(@Param("id") int id);
    //当前学生课题
    public List<TopicUnit> getMyTopic(Map<String,Object> map);
}
