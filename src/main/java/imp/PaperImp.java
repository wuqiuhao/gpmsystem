package imp;

import entity.Paper;
import entity.PaperUnit;

import java.util.List;
import java.util.Map;

/**
 * @author wqh
 * @date 2021/3/8 16:46
 */
public interface PaperImp {
    /**
     * 查询列表
     * @return
     */
    public List<PaperUnit> getAllPaperInfo(Map<String,Object> map);
    /**
     * 查询总数
     * @return
     */
    public int getCountPaper();
    //上传论文
    public String subPaper(Paper paper);
}
