package imp;

import com.alibaba.fastjson.JSONObject;
import org.springframework.web.multipart.MultipartFile;

import java.util.Map;

/**
 * @author wqh
 * @date 2021/3/16 16:48
 */
public interface UploadService {
    JSONObject uploadWord(MultipartFile file, String type,String id,String grade, String realPath);
}
