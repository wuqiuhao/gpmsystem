package imp;

import entity.Student;
import entity.Teacher;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author wqh
 * @date 2020/12/8 20:39
 */
public interface UserImp {
    /**
     * 登录判断
     * @param user
     * @return
     */
    public String StudentLogin(@Param("studentNo") String studentNo,@Param("studentPassword") String studentPassword);
    public String TeacherLogin(@Param("teacherNo") String teacherNo,@Param("teacherPassword") String teacherPassword);
    /**
     * 修改当前用户密码
     * @param userPassword
     * @return
     */
    public String updateStudentPsw(String studentNo,String studentPassword);

    public String updateTeacherPsw(String teacherNo,String teacherPassword);

    /**
     *  当前用户信息查询
     * @return
     */
    public Student getStudentInfo(String studentNo);

    public Teacher getTeacherInfo(String teacherNo);

    //查询教师ID
    public int getTeacherID(String teacherNo);

    //获取当前学生对应的教师ID
    public int getTeacherIdByNo(String studentNo);

    //获取当前学生ID
    public int getStudentID(String studentNo);
}
