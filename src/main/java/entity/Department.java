package entity;

/**
 * @author wqh
 * @date 2020/12/16 19:51
 * 学院实体类
 */
public class Department {
    private int dpm_id;
    private String departmentName;
    private String description;

    public int getId() {
        return dpm_id;
    }

    public void setId(int dpm_id) {
        this.dpm_id = dpm_id;
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
