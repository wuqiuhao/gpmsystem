package entity;

import java.util.Date;

/**
 * @author wqh
 * @date 2020/12/8 17:07
 * 学生信息实体类
 */
public class Student {
    private int id;
    private String studentNo;
    private String studentName;
    private String studentPassword;
    private int sex;
    private int majorId;
    private int gradeId;
    private int degreeId;
    private int teacherId;
    private String phone;
    private String email;
    private String QQ;
    private Date lastModifyTime;

    public String getStudentPassword() {
        return studentPassword;
    }

    public void setStudentPassword(String studentPassword) {
        this.studentPassword = studentPassword;
    }

    public int getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(int teacherId) {
        this.teacherId = teacherId;
    }

    public Date getLastModifyTime() {
        return lastModifyTime;
    }

    public void setLastModifyTime(Date lastModifyTime) {
        this.lastModifyTime = lastModifyTime;
    }

    public String getQQ() {
        return QQ;
    }

    public void setQQ(String QQ) {
        this.QQ = QQ;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStudentNo() {
        return studentNo;
    }

    public void setStudentNo(String studentNo) {
        this.studentNo = studentNo;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public int getSex() {
        return sex;
    }

    public void setSex(int sex) {
        this.sex = sex;
    }

    public int getMajorId() {
        return majorId;
    }

    public void setMajorId(int majorId) {
        this.majorId = majorId;
    }

    public int getGrade() {
        return gradeId;
    }

    public void setGrade(int gradeId) {
        this.gradeId = gradeId;
    }

    public int getDegree() {
        return degreeId;
    }

    public void setDegree(int degreeId) {
        this.degreeId = degreeId;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
