package entity;

/**
 * @author wqh
 * @date 2021/3/15 20:32
 */
public class TopicUnit {
    private int id;
    private String thesisName;
    private String studentId;
    private String grade;
    private String s_name;
    private int s_id;
    private String taskBookName;
    private String taskBookPath;
    private int is_upload;

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public String getS_name() {
        return s_name;
    }

    public void setS_name(String s_name) {
        this.s_name = s_name;
    }

    public String getStudentId() {
        return studentId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getThesisName() {
        return thesisName;
    }

    public void setThesisName(String thesisName) {
        this.thesisName = thesisName;
    }

    public int getS_id() {
        return s_id;
    }

    public void setS_id(int s_id) {
        this.s_id = s_id;
    }

    public String getTaskBookName() {
        return taskBookName;
    }

    public void setTaskBookName(String taskBookName) {
        this.taskBookName = taskBookName;
    }

    public String getTaskBookPath() {
        return taskBookPath;
    }

    public void setTaskBookPath(String taskBookPath) {
        this.taskBookPath = taskBookPath;
    }

    public int getIs_upload() {
        return is_upload;
    }

    public void setIs_upload(int is_upload) {
        this.is_upload = is_upload;
    }
}
