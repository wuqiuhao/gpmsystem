package entity;

import java.util.Date;

/**
 * @author wqh
 * @date 2021/3/8 16:08
 */
public class Topic {
    private int id;
    private String thesisName;
    private String taskBookName;
    private int teacherId;
    private Date submitTime;
    private int is_upload;
    private int s_id;
    private String thesisPath;
    private String taskBookPath;

    public int getIs_upload() {
        return is_upload;
    }

    public void setIs_upload(int is_upload) {
        this.is_upload = is_upload;
    }

    public int getS_id() {
        return s_id;
    }

    public void setS_id(int s_id) {
        this.s_id = s_id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getThesisName() {
        return thesisName;
    }

    public void setThesisName(String thesisName) {
        this.thesisName = thesisName;
    }

    public String getTaskBookName() {
        return taskBookName;
    }

    public void setTaskBookName(String taskBookName) {
        this.taskBookName = taskBookName;
    }

    public int getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(int teacherId) {
        this.teacherId = teacherId;
    }

    public Date getSubmitTime() {
        return submitTime;
    }

    public void setSubmitTime(Date submitTime) {
        this.submitTime = submitTime;
    }

    public String getThesisPath() {
        return thesisPath;
    }

    public void setThesisPath(String thesisPath) {
        this.thesisPath = thesisPath;
    }

    public String getTaskBookPath() {
        return taskBookPath;
    }

    public void setTaskBookPath(String taskBookPath) {
        this.taskBookPath = taskBookPath;
    }
}
