package entity;

/**
 * @author wqh
 * @date 2021/2/26 12:23
 */
public class Grade {
    private int id;
    private String grade;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }
}
