package entity;

/**
 * @author wqh
 * @date 2021/3/16 16:13
 */
public class PaperUnit {
    private int id;
    private String studentName;
    private String teacherName;
    private String paperName;
    private String paperdescription;
    private int isUpload;
    private float version;
    private int sid;
    private int tid;
    private String paperaddress;

    public String getPaperaddress() {
        return paperaddress;
    }

    public void setPaperaddress(String paperaddress) {
        this.paperaddress = paperaddress;
    }

    public int getSid() {
        return sid;
    }

    public void setSid(int sid) {
        this.sid = sid;
    }

    public int getTid() {
        return tid;
    }

    public void setTid(int tid) {
        this.tid = tid;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public String getTeacherName() {
        return teacherName;
    }

    public void setTeacherName(String teacherName) {
        this.teacherName = teacherName;
    }

    public String getPaperName() {
        return paperName;
    }

    public void setPaperName(String paperName) {
        this.paperName = paperName;
    }

    public String getPaperdescription() {
        return paperdescription;
    }

    public void setPaperdescription(String paperdescription) {
        this.paperdescription = paperdescription;
    }

    public int getIsUpload() {
        return isUpload;
    }

    public void setIsUpload(int isUpload) {
        this.isUpload = isUpload;
    }

    public float getVersion() {
        return version;
    }

    public void setVersion(float version) {
        this.version = version;
    }
}
