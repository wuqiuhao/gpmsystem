package entity;

import java.util.Date;

/**
 * @author wqh
 * @date 2021/3/7 22:29
 */
public class Paper {
    private int id;
    private int studentId;
    private String paperName;
    private String paperdescription;
    private Date submitTime;
    private int isUpload;
    private float version;
    private String paperaddress;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getStudentId() {
        return studentId;
    }

    public void setStudentId(int studentId) {
        this.studentId = studentId;
    }

    public String getPaperName() {
        return paperName;
    }

    public void setPaperName(String paperName) {
        this.paperName = paperName;
    }

    public String getPaperdescription() {
        return paperdescription;
    }

    public void setPaperdescription(String paperdescription) {
        this.paperdescription = paperdescription;
    }

    public Date getSubmitTime() {
        return submitTime;
    }

    public void setSubmitTime(Date submitTime) {
        this.submitTime = submitTime;
    }

    public int getIsUpload() {
        return isUpload;
    }

    public void setIsUpload(int isUpload) {
        this.isUpload = isUpload;
    }

    public float getVersion() {
        return version;
    }

    public void setVersion(float version) {
        this.version = version;
    }

    public String getPaperaddress() {
        return paperaddress;
    }

    public void setPaperaddress(String paperaddress) {
        this.paperaddress = paperaddress;
    }
}
