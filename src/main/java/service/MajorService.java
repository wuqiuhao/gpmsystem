package service;

import dao.MajorDao;
import entity.Major;
import imp.MajorImp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @author wqh
 * @date 2021/1/10 15:34
 */
@Service
public class MajorService implements MajorImp {

    @Autowired
    private MajorDao majorDao;

    @Override
    public List<Major> getAllMajor(Map<String,Object> map) {
        return majorDao.getAllMajor(map);
    }

    @Override
    public List<Major> getMajorList() {
        return majorDao.getMajorList();
    }

    @Override
    public int getCountMajor() {
        return majorDao.getCountMajor();
    }

    @Override
    public String insertMajorInfo(Major major) {
        int count = majorDao.insertMajorInfo(major);
        if (count > 0){
            return "1";
        }else {
            return "新增失败！";
        }
    }

    @Override
    public String updateMajorInfo(Major major) {
        int count = majorDao.updateMajorInfo(major);
        if (count > 0){
            return "1";
        }else {
            return "修改失败！";
        }
    }

    @Override
    public String delMajorInfo(int id) {
        int count = majorDao.delMajorInfo(id);
        if (count > 0){
            return "1";
        }else {
            return "删除失败！";
        }
    }
}
