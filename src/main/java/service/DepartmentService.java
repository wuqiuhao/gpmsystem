package service;

import dao.DepartmentDao;
import entity.Department;
import imp.DepartmentImp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @author wqh
 * @date 2020/12/16 20:05
 */
@Service("DepartmentImp")
public class DepartmentService implements DepartmentImp {

    @Autowired
    private DepartmentDao departmentDao;

    @Override
    public List<Department> getAllDepartment(Map<String,Object> map) {
        return departmentDao.getAllDepartment(map);
    }

    @Override
    public List<Department> getDpmList() {
        return departmentDao.getDpmList();
    }

    @Override
    public int getCountDepartment() {
        return departmentDao.getCountDepartment();
    }

    @Override
    public String insertDepartmentInfo(Department department) {
        int count = departmentDao.insertDepartmentInfo(department);
        if (count > 0){
            return "1";
        }else {
            return "新增失败！";
        }
    }

    @Override
    public String updateDepartmentInfo(Department department) {
        int count = departmentDao.updateDepartmentInfo(department);
        if (count > 0){
            return "1";
        }else {
            return "修改失败！";
        }
    }

    @Override
    public String delDeparmentInfo(int dpm_id) {
        int count = departmentDao.delDeparmentInfo(dpm_id);
        if (count > 0){
            return "1";
        }else {
            return "删除失败！";
        }
    }
}
