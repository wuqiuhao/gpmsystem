package service;

import dao.BasicInfoDao;
import entity.Grade;
import entity.Notice;
import imp.BasicInfoImp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @author wqh
 * @date 2021/1/6 21:23
 */
@Service("BasicInfoImp")
public class BasicInfoService implements BasicInfoImp{

    @Autowired
    private BasicInfoDao basicInfoDao;

    @Override
    public List<Notice> getAllNoticeInfo(Map<String, Object> map) {
        return basicInfoDao.getAllNoticeInfo(map);
    }

    @Override
    public int getNoticeCount() {
        return basicInfoDao.getNoticeCount();
    }

    @Override
    public String updateNoticeInfo(Notice notice) {
        int count = basicInfoDao.updateNoticeInfo(notice);
        if (count > 0){
            return "1";
        }else {
            return "修改失败！";
        }
    }

    @Override
    public String insertNoticeInfo(Notice notice) {
        int count = basicInfoDao.insertNoticeInfo(notice);
        if (count > 0){
            return "1";
        }else {
            return "新增失败！";
        }
    }

    @Override
    public String delNoticeInfo(int id) {
        int count = basicInfoDao.delNoticeInfo(id);
        if (count > 0) {
            return "1";
        } else {
            return "删除失败！";
        }
    }


    @Override
    public List<Grade> findAllGrade(Map<String, Object> map) {
        return basicInfoDao.findAllGrade(map);
    }

    @Override
    public List<Grade> getGradeList() {
        return basicInfoDao.getGradeList();
    }

    @Override
    public int getGradeCount() {
        return basicInfoDao.getGradeCount();
    }

    @Override
    public String insertGradeInfo(Grade grade) {
        int count = basicInfoDao.insertGradeInfo(grade);
        if (count > 0){
            return "1";
        }else {
            return "新增失败！";
        }
    }

    @Override
    public String updateGradeInfo(Grade grade) {
        int count = basicInfoDao.updateGradeInfo(grade);
        if (count > 0){
            return "1";
        }else {
            return "修改失败！";
        }
    }

    @Override
    public String delGradeInfo(int id) {
        int count = basicInfoDao.delGradeInfo(id);
        if (count > 0){
            return "1";
        }else {
            return "删除失败！";
        }
    }
}
