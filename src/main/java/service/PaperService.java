package service;

import dao.PaperDao;
import entity.Paper;
import entity.PaperUnit;
import imp.PaperImp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @author wqh
 * @date 2021/3/8 16:50
 */
@Service("PaperImp")
public class PaperService implements PaperImp {
    @Autowired
    private PaperDao paperDao;

    @Override
    public List<PaperUnit> getAllPaperInfo(Map<String, Object> map) {
        return paperDao.getAllPaperInfo(map);
    }

    @Override
    public int getCountPaper() {
        return paperDao.getCountPaper();
    }

    @Override
    public String subPaper(Paper paper) {
        int count = paperDao.subPaper(paper);
        if (count > 0){
            return "1";
        }else {
            return "上传失败！";
        }
    }
}
