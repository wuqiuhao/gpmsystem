package service;

import dao.TeacherDao;
import entity.Student;
import entity.Teacher;
import entity.TeacherUnit;
import imp.TeacherImp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @author wqh
 * @date 2020/12/19 20:22
 */
@Service("TeacherImp")
public class TeacherService implements TeacherImp {

    @Autowired
    private TeacherDao teacherDao;

    @Override
    public List<TeacherUnit> getAllTeacherInfo(Map<String, Object> map) {
        return teacherDao.getAllTeacherInfo(map);
    }

    @Override
    public int getCountTeacher() {
        return teacherDao.getCountTeacher();
    }

    @Override
    public List<Teacher> getTeacherList() {
        return teacherDao.getTeacherList();
    }

    @Override
    public String insertTeacherInfo(Teacher teacher) {
        int count = teacherDao.insertTeacherInfo(teacher);
        if (count > 0) {
            return "1";
        } else {
            return "新增失败！";
        }
    }

    @Override
    public String updateTeacherInfo(Teacher teacher) {
        int count = teacherDao.updateTeacherInfo(teacher);
        if (count > 0) {
            return "1";
        } else {
            return "修改失败！";
        }
    }

    @Override
    public String delTeacherInfo(int id) {
        int count = teacherDao.delTeacherInfo(id);
        if (count > 0) {
            return "1";
        } else {
            return "删除失败！";
        }
    }
}
