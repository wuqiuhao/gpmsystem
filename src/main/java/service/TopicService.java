package service;

import dao.TopicDao;
import entity.Topic;
import entity.TopicUnit;
import imp.TopicImp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @author wqh
 * @date 2021/3/8 16:35
 */
@Service("TopicImp")
public class TopicService implements TopicImp {

    @Autowired
    private TopicDao topicDao;

    @Override
    public List<TopicUnit> getAllTopicInfo(Map<String, Object> map) {
        return topicDao.getAllTopicInfo(map);
    }

    @Override
    public int getCountTopic() {
        return topicDao.getCountTopic();
    }

    @Override
    public String addTopic(Topic topic) {
        int count = topicDao.addTopic(topic);
        if (count > 0){
            return "1";
        }else {
            return "新增失败！";
        }
    }

    @Override
    public String updateTopic(Topic topic) {
        int count = topicDao.updateTopic(topic);
        if (count > 0){
            return "1";
        }else {
            return "修改失败！";
        }
    }

    @Override
    public String delTopic(int id) {
        int count = topicDao.delTopic(id);
        if (count > 0){
            return "1";
        }else {
            return "删除失败！";
        }
    }

    @Override
    public List<TopicUnit> getMyTopic(Map<String,Object> map) {
        return topicDao.getMyTopic(map);
    }
}
