package service;

import dao.ReportDao;
import entity.Report;
import entity.ReportUnit;
import imp.ReportImp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @author wqh
 * @date 2021/3/8 16:21
 */
@Service("ReportImp")
public class ReportService implements ReportImp {

    @Autowired
    private ReportDao reportDao;

    @Override
    public List<ReportUnit> getAllReportInfo(Map<String, Object> map) {
        return reportDao.getAllReportInfo(map);
    }

    @Override
    public int getCountReport() {
        return reportDao.getCountReport();
    }

    @Override
    public String insertReport(Report report) {
        int count = reportDao.insertReport(report);
        if (count > 0) {
            return "1";
        } else {
            return "新增失败！";
        }
    }

    @Override
    public String updateReport(Report report) {
        int count = reportDao.updateReport(report);
        if (count > 0) {
            return "1";
        } else {
            return "修改失败！";
        }
    }

    @Override
    public String delReport(int id) {
        int count = reportDao.delReport(id);
        if (count > 0) {
            return "1";
        } else {
            return "删除失败！";
        }
    }

    @Override
    public String subReport(Report report) {
        int count = reportDao.subReport(report);
        if (count > 0) {
            return "1";
        } else {
            return "上传失败！";
        }
    }
}
