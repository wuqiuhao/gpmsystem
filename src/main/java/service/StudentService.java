package service;

import dao.StudentDao;
import entity.Student;
import entity.StudentUnit;
import imp.StudentImp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @author wqh
 * @date 2020/12/16 22:25
 */
@Service("StudentImp")
public class StudentService implements StudentImp {
    @Autowired
    private StudentDao studentDao;

    @Override
    public List<StudentUnit> findAllStudents(Map<String, Object> map) {
        return studentDao.findAllStudents(map);
    }

    @Override
    public int getStudentCount() {
        return studentDao.getStudentCount();
    }

    @Override
    public String insertStudentInfo(Student student) {
        int count = studentDao.insertStudentInfo(student);
        if (count > 0) {
            return "1";
        } else {
            return "插入失败！";
        }
    }

    @Override
    public String updateStudentInfo(Student student) {
        int count = studentDao.updateStudentInfo(student);
        if (count > 0) {
            return "1";
        } else {
            return "修改失败！";
        }
    }

    @Override
    public String delStudentInfo(int id) {
        int count = studentDao.delStudentInfo(id);
        if (count > 0) {
            return "1";
        } else {
            return "删除失败！";
        }
    }

    @Override
    public List<Student> getStudentList(int teacherId) {
        return studentDao.getStudentList(teacherId);
    }
}
