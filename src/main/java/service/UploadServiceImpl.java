package service;

import com.alibaba.fastjson.JSONObject;
import dao.PaperDao;
import dao.ReportDao;
import dao.TopicDao;
import entity.Paper;
import entity.Report;
import entity.Topic;
import imp.UploadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import util.WordToHtml;

import java.io.File;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * @author wqh
 * @date 2021/3/16 16:49
 */
@Service
public class UploadServiceImpl implements UploadService {

    @Autowired
    private PaperDao paperDao;

    @Autowired
    private ReportDao reportDao;

    @Autowired
    private TopicDao topicDao;

    @Override
    public JSONObject uploadWord(MultipartFile file, String type,String id,String grade, String realPath) {
        JSONObject object = new JSONObject();
        try {
        if ("taskbook".equals(type)) {
            realPath += "/" + type + "/" + id + "/" + grade + "/";
        } else {
            realPath += "/" + type + "/" + grade + "/" + id + "/";
        }
        String fileName = file.getOriginalFilename();
        float version = 0;
        if (!"taskbook".equals(type)) {
            File versionList = new File(realPath);
            versionList.mkdirs();
            version = Objects.requireNonNull(versionList.list()).length + 1;
            realPath += version + "/";
        }
        String filePath = realPath + fileName;
        File f = new File(filePath);
        f.mkdirs();

        String htmlName = fileName.replaceFirst(fileName
                .substring(fileName.lastIndexOf(".")), ".html");

        String htmlPath = realPath + htmlName;
        String htmlAddress = "/" + htmlPath.substring(filePath.lastIndexOf("word"));

            file.transferTo(f);
            new Thread(() ->
                    WordToHtml.wordToHtml(filePath, htmlPath)).start();

            object.put("msg", version);
            object.put("code", 1);
            object.put("data", htmlAddress);
            return object;
        } catch (Exception e) {
            System.out.println("word转化失败");
            object.put("msg", "上传失败");
            object.put("code", 0);
            object.put("data", "");
            return object;
        }


       /* switch(type) {
            case "paper": {
                Paper paper = new Paper();
                paper.setStudentId(1);                                     // 学生id
                paper.setPaperName("111111111");                           // 论文名字
                paper.setSubmitTime(new Date());
                paper.setVersion(version);
                paper.setPaperaddress(htmlAddress);
                if (paperDao.subPaper(paper) <= 0) {
                    tag = "0";
                }
            }break;
            case "report": {
                Report report = new Report();
                report.setSubmitTime(new Date());
                report.setReportAddress(htmlAddress);
                report.setVersion(version);
                if (reportDao.subReport(report) <= 0) {
                    tag = "0";
                }
            }break;
            default: {
                int topicId = 3;                                     // 课题id
                Topic topic = new Topic();
                topic.setId(topicId);
                topic.setSubmitTime(new Date());
                topic.setTaskBookName(fileName.substring(0, fileName.lastIndexOf(".")));
                topic.setTaskBookPath(htmlAddress);
                if (topicDao.subTopic(topic) <= 0) {
                    tag = "0";
                }
            }
        }*/

    }
}
