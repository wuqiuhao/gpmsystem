package service;

import dao.UserDao;
import entity.Student;
import entity.Teacher;
import imp.UserImp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author wqh
 * @date 2020/12/8 17:38
 */
@Service
public class UserService implements UserImp {

    @Autowired
    private UserDao userDao;

    //登录判断
    @Override
    public String StudentLogin(String studentNo, String studentPassword) {
           int count = userDao.StudentLogin(studentNo,studentPassword);
        if (count > 0){
            return "2";
        }else
            return "登陆失败！";
    }

    @Override
    public String TeacherLogin(String teacherNo, String teacherPassword) {
        int count = userDao.TeacherLogin(teacherNo,teacherPassword);
        if (count > 0){
            return "0";
        }else
            return "登录失败！";
    }

    @Override
    public String updateStudentPsw(String studentNo, String studentPassword) {
        int count = userDao.updateStudentPsw(studentNo,studentPassword);
        if (count > 0){
            return "1";
        }else
            return "修改失败！";
    }

    @Override
    public String updateTeacherPsw(String teacherNo, String teacherPassword) {
        int count = userDao.updateTeacherPsw(teacherNo,teacherPassword);
        if (count > 0){
            return "1";
        }else
            return "修改失败！";
    }


    //当前用户信息查询
    @Override
    public Student getStudentInfo(String studentNo) {
        return userDao.getStudentInfo(studentNo);
    }

    @Override
    public Teacher getTeacherInfo(String teacherNo) {
        return userDao.getTeacherInfo(teacherNo);
    }

    @Override
    public int getTeacherID(String teacherNo) {
        return userDao.getTeacherID(teacherNo);
    }

    @Override
    public int getTeacherIdByNo(String studentNo) {
        return userDao.getTeacherIdByNo(studentNo);
    }

    @Override
    public int getStudentID(String studentNo) {
        return userDao.getStudentID(studentNo);
    }

}
