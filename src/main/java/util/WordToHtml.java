package util;

import com.spire.doc.Document;
import com.spire.doc.FileFormat;

public class WordToHtml {

    public static void wordToHtml(String wordPath, String htmlPath) {
        Document document = new Document(wordPath);
        document.saveToFile(htmlPath, FileFormat.Html);
    }
}