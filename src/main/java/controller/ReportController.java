package controller;

import entity.Department;
import entity.Report;
import entity.ReportUnit;
import entity.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import service.ReportService;

import javax.servlet.http.HttpSession;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author wqh
 * @date 2021/3/8 16:23
 */
@Controller
public class ReportController {
    @Autowired
    private ReportService reportService;

    @RequestMapping("/reportInfo")
    public String ReportInfo(){
        return "basicInfo/reportInfo";
    }

    @RequestMapping("/taskBookInfo")
    public String TaskbookInfo(){
        return "basicInfo/taskBookInfo";
    }

    @RequestMapping("/uploadReport")
    public String uploadReport(){
        return "report/uploadReport";
    }

    @RequestMapping("/reportScoring")
    public String reportScoring(){
        return "report/reportScoring";
    }

    //课题列表controller
    @RequestMapping("/getReportInfo")
    @ResponseBody
    public Map<String,Object> getReportInfo(int page, int limit,String thesisName,HttpSession session){
        Map<String,Object> map = new HashMap<String, Object>();
        //获取当前页号
        int curr = (page-1)*limit;
        map.put("page",curr);
        map.put("limit",limit);
        map.put("thesisName",thesisName);
        Object tid = session.getAttribute("teacherID");
        if (tid!=null){
            String t_id = tid.toString();
            map.put("tid",Integer.parseInt(t_id));
        }else {
            map.put("tid",null);
        }
        Object sid = session.getAttribute("sid");
        if (sid!=null){
            String s_id = sid.toString();
            map.put("sid",Integer.parseInt(s_id));
        }else {
            map.put("sid",null);
        }
        List<ReportUnit> reportUnits = reportService.getAllReportInfo(map);
        int totals = reportService.getCountReport();
        Map<String, Object> maps = new HashMap <String, Object>();
        maps.put("msg", "查找成功！");
        maps.put("code", 0);
        maps.put("count", totals);
        maps.put("data", reportUnits);
        return maps;
    }
    //新增开题报告
    @RequestMapping("/addReport")
    @ResponseBody
    public String addReport(Integer id,String reportAddress,String description,Float version){
        Report report = new Report();
        report.setTopicId(id);
        report.setIsSubmit(1);
        report.setDescription(description);
        report.setReportAddress(reportAddress);
        report.setVersion(version);
        Date date = new Date();
        report.setSubmitTime(date);
        String msg = reportService.subReport(report);
        return msg;
    }
    //修改开题报告
    @RequestMapping("/editReport")
    @ResponseBody
    public String editReport(Integer id,String reportAddress,String rater){
        Report report = new Report();
        report.setId(id);
        report.setReportAddress(reportAddress);
        Date date = new Date();
        report.setRater(rater);
        report.setSubmitTime(date);
        String msg = reportService.insertReport(report);
        return msg;
    }
    //教师打分
    @RequestMapping("/scoring")
    @ResponseBody
    public String Scoring(int id,String studentScore,String rater){
        Report report = new Report();
        report.setId(id);
        report.setStudentScore(studentScore);
        Date date = new Date();
        report.setRater(rater);
        report.setSubmitTime(date);
        String msg = reportService.updateReport(report);
        return msg;
    }
}
