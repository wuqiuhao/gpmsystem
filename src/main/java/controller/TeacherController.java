package controller;

import entity.Department;
import entity.Student;
import entity.Teacher;
import entity.TeacherUnit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import service.TeacherService;

import javax.servlet.http.HttpSession;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author wqh
 * @date 2020/12/16 11:34
 */
@Controller
public class TeacherController {

    @Autowired
    private TeacherService teacherService;
    @RequestMapping("/teacherList")
    public String teacherInfo(){
        return "basicInfo/teacherList";
    }
    //获取下拉列表
    @RequestMapping("/getTeacherList")
    @ResponseBody
    public Map<String,Object> getDpmList(){
        List<Teacher> teachers = teacherService.getTeacherList();
        Map<String, Object> maps = new HashMap <String, Object>();
        maps.put("msg", "查找成功！");
        maps.put("code", 0);
        maps.put("count", "");
        maps.put("data", teachers);
        return maps;
    }
    //查询教师列表
    @RequestMapping("/getAllTeacher")
    @ResponseBody
    public Map<String,Object> getAllTeacher(int page,int limit,String teacherName){
        Map<String,Object> map = new HashMap<String, Object>();
        //获取当前页号
        int curr = (page-1)*limit;
        map.put("page",curr);
        map.put("limit",limit);
        map.put("teacherName",teacherName);
        Map<String,Object> maps = new HashMap<String, Object>();
        List<TeacherUnit> teachers = teacherService.getAllTeacherInfo(map);
        int totals = teacherService.getCountTeacher();
        maps.put("msg", "查找成功！");
        maps.put("code", 0);
        maps.put("count", totals);
        maps.put("data", teachers);
        return maps;
    }
    //新增教师信息
    @RequestMapping("/insertTeacher")
    public String insertTeacher(){
        return "teacher/addTeacher";
    }
    @RequestMapping("/addTeacher")
    @ResponseBody
    public String addTeacher(String teacherNo,String teacherName,Integer sex,int departmentId,String phone,String email,String qQ){
        Teacher teacher = new Teacher();
        teacher.setTeacherNo(teacherNo);
        teacher.setTeacherName(teacherName);
        teacher.setTeacherPassword("888");
        teacher.setSex(sex);
        teacher.setDepartmentId(departmentId);
        teacher.setQQ(qQ);
        teacher.setEmail(email);
        teacher.setPhone(phone);
        Date date = new Date();
        teacher.setLastModifyTime(date);
        String msg = teacherService.insertTeacherInfo(teacher);
        return msg;
    }
    //编辑教师信息
    @RequestMapping("/editTeacher")
    public String editTeacher(){
        return "teacher/editTeacher";
    }
    @RequestMapping("/updateTeacher")
    @ResponseBody
    public String updateTeacher(Integer id,String teacherNo,String teacherName,Integer sex,int departmentId,Integer teacherId,String phone,String email,String qQ){
        Teacher teacher = new Teacher();
        teacher.setId(id);
        teacher.setTeacherNo(teacherNo);
        teacher.setTeacherName(teacherName);
        teacher.setSex(sex);
        teacher.setQQ(qQ);
        teacher.setEmail(email);
        teacher.setDepartmentId(departmentId);
        teacher.setPhone(phone);
        Date date = new Date();
        teacher.setLastModifyTime(date);
        String msg = teacherService.updateTeacherInfo(teacher);
        return msg;
    }
    //删除学生
    @RequestMapping("/delTeacher")
    @ResponseBody
    public String DelDepartment(Integer id){
        String msg = teacherService.delTeacherInfo(id);
        return msg;
    }
    //查看教师信息
    @RequestMapping("/teacherDetail")
    public String detailTeacher(){
        return "teacher/teacherDetail";
    }
}
