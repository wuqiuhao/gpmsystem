package controller;

import entity.Department;
import entity.Grade;
import entity.Notice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import service.BasicInfoService;

import javax.security.auth.Subject;
import javax.xml.crypto.Data;
import java.text.DateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author wqh
 * @date 2021/1/6 21:10
 */
@Controller
public class BasicInfoController {

    @Autowired
    private BasicInfoService basicInfoService;
    //公告信息
    @RequestMapping("/noticeInfo")
    public String noticeInfo() {
        return "basicInfo/noticeInfo";
    }

    //上传
    @RequestMapping("/upload")
    public String Upload() {
        return "upload";
    }

    //添加公告
    @RequestMapping("/insertNotice")
    public String insertNotice(){
        return "notice/insertNotice";
    }

    //编辑公告
    @RequestMapping("/noticeEdit")
    public String NoticeEdit(){
        return "notice/noticeEdit";
    }

    //查看公告
    @RequestMapping("/noticeDetail")
    public String noticeDetail(){
        return "notice/noticeDetail";
    }
    //教师首页
    @RequestMapping("/welcome-t")
    public String Twelcome() {
        return "basicInfo/welcome-t";
    }
    //公告列表信息
    @RequestMapping("/getNoticeInfo")
    @ResponseBody
    public Map<String,Object> getNoticeInfo(int page,int limit,String title){
        Map<String,Object> map = new HashMap<String, Object>();
        //获取当前页号
        int curr = (page-1)*limit;
        map.put("page",curr);
        map.put("limit",limit);
        map.put("title",title);
        List<Notice> notices = basicInfoService.getAllNoticeInfo(map);
        int totals = basicInfoService.getNoticeCount();
        Map<String, Object> maps = new HashMap<String, Object>();
        maps.put("msg", "查找成功！");
        maps.put("code", 0);
        maps.put("count", totals);//总数后面改成sql接口查询
        maps.put("data", notices);
        return maps;
    }
    //新增公告
    @RequestMapping("/addNotice")
    @ResponseBody
    public String addNotice(String title,String context){
        Notice notice = new Notice();
        notice.setTitle(title);
        notice.setContext(context);
        Date date = new Date();
        notice.setLastModifyTime(date);
        String msg = basicInfoService.insertNoticeInfo(notice);
        return msg;
    }
    //修改公告
    @RequestMapping("/updateNotice")
    @ResponseBody
    public String UpdateNotice(String title,String context,int id){
        Notice notice = new Notice();
        notice.setTitle(title);
        notice.setContext(context);
        notice.setId(id);
        String msg = basicInfoService.updateNoticeInfo(notice);
        return msg;
    }
    //删除公告
    @RequestMapping("/delNotice")
    @ResponseBody
    public String DelScore(int id){
        String msg = basicInfoService.delNoticeInfo(id);
        return msg;
    }
    //学届信息
   @RequestMapping("/academicInfo")
   public String academicInfo(){
        return "basicInfo/academicInfo";
   }
    //获取下拉列表
    @RequestMapping("/getGradeList")
    @ResponseBody
    public Map<String,Object> getGradeList(){
        List<Grade> grades = basicInfoService.getGradeList();
        Map<String, Object> maps = new HashMap <String, Object>();
        maps.put("msg", "查找成功！");
        maps.put("code", 0);
        maps.put("count", "");
        maps.put("data", grades);
        return maps;
    }
    //学届列表信息
    @RequestMapping("/getGradeInfo")
    @ResponseBody
    public Map<String,Object> getGradeInfo(int page,int limit,String grade){
        Map<String,Object> map = new HashMap<String, Object>();
        //获取当前页号
        int curr = (page-1)*limit;
        map.put("page",curr);
        map.put("limit",limit);
        map.put("grade",grade);
        List<Grade> grades = basicInfoService.findAllGrade(map);
        Map<String, Object> maps = new HashMap<String, Object>();
        maps.put("msg", "查找成功！");
        maps.put("code", 0);
        maps.put("count", grades.size());//总数后面改成sql接口查询
        maps.put("data", grades);
        return maps;
    }
    //学届添加
    @RequestMapping("/addAcademic")
    public String academicAdd(){
        return "academic/addAcademic";
    }

    @RequestMapping("/addGrade")
    @ResponseBody
    public String addGrade(String grade){
        Grade grades = new Grade();
        grades.setGrade(grade);
        String msg = basicInfoService.insertGradeInfo(grades);
        return msg;
    }
    //学届修改
    @RequestMapping("/academicEdit")
    public String academicEdit(){ return "academic/academicEdit"; }

    @RequestMapping("/updateGrade")
    @ResponseBody
    public String updateGrade(int id,String grade){
        Grade grades = new Grade();
        grades.setId(id);
        grades.setGrade(grade);
        String msg = basicInfoService.updateGradeInfo(grades);
        return msg;
    }
    //学届删除
    @RequestMapping("/delGrade")
    @ResponseBody
    public String delGrade(int id){
        String msg = basicInfoService.delGradeInfo(id);
        return msg;
    }
    //学届详情
    @RequestMapping("/academicDetail")
    public String academicDetail(){ return "academic/academicDetail"; }
}
