package controller;

import entity.Topic;
import entity.TopicUnit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import service.TopicService;

import javax.servlet.http.HttpSession;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author wqh
 * @date 2021/3/8 16:43
 */
@Controller
public class TopicController {
    @Autowired
    private TopicService topicService;

    @RequestMapping("/topicInfo")
    public String ReportInfo(){
        return "basicInfo/topicInfo";
    }

    //课题列表controller
    @RequestMapping("/getTopicInfo")
    @ResponseBody
    public Map<String,Object> getTopicInfo(int page, int limit,String thesisName, HttpSession session){
        Map<String,Object> map = new HashMap<String, Object>();
        //获取当前页号
        int curr = (page-1)*limit;
        map.put("page",curr);
        map.put("limit",limit);
        map.put("thesisName",thesisName);
        map.put("id",session.getAttribute("teacherID"));
        List<TopicUnit> topics = topicService.getAllTopicInfo(map);
        int totals = topicService.getCountTopic();
        Map<String, Object> maps = new HashMap <String, Object>();
        maps.put("msg", "查找成功！");
        maps.put("code", 0);
        maps.put("count", totals);
        maps.put("data", topics);
        return maps;
    }
    //添加开题报告
    @RequestMapping("/addTopic")
    public String addTopic(){
        return "topic/addTopic";
    }
    @RequestMapping("/insertTopic")
    @ResponseBody
    public String insertTopic(String thesisName,Integer s_id,HttpSession session){
        Topic topic = new Topic();
        topic.setThesisName(thesisName);
        topic.setIs_upload(0);
        topic.setS_id(s_id);
        String teacherId = session.getAttribute("teacherID").toString();
        topic.setTeacherId(Integer.parseInt(teacherId));
        String msg = topicService.addTopic(topic);
        return msg;
    }
    //上传任务书
    @RequestMapping("/uploadTaskBook")
    public String uploadTaskBook(){
        return "topic/uploadTaskBook";
    }
    //修改
    @RequestMapping("/uploadTopic")
    public String eidtTopic(){
        return "topic/editTopic";
    }
    //详情
    @RequestMapping("/myTopic")
    public String myTopic(){
        return "topic/myTopic";
    }
    //详情
    @RequestMapping("/topicDetail")
    public String topicDetail(){
        return "topic/topicDetail";
    }
    //修改开题报告
    @RequestMapping("/editTopic")
    public String editTopic(){
        return "topic/editTopic";
    }
    @RequestMapping("/updateTopic")
    @ResponseBody
    public String updateTopic(Integer id,String thesisName,String taskBookName,String taskBookPath){
        Topic topic = new Topic();
        topic.setId(id);
        topic.setThesisName(thesisName);
        topic.setTaskBookName(taskBookName);
        topic.setTaskBookPath(taskBookPath);
        topic.setIs_upload(1);
        Date date = new Date();
        topic.setSubmitTime(date);
        String msg = topicService.updateTopic(topic);
        return msg;
    }
    //删除课题
    @RequestMapping("/delTopic")
    @ResponseBody
    public String DelMajor(int id){
        String msg = topicService.delTopic(id);
        return msg;
    }
    //学生课题
    @RequestMapping("/myTopicInfo")
    @ResponseBody
    public Map<String,Object> myTopicInfo(String thesisName, HttpSession session){
        Map<String,Object> map = new HashMap<String, Object>();
        map.put("s_id",session.getAttribute("sid"));
        List<TopicUnit> topics = topicService.getMyTopic(map);
        int totals = topics.size();
        Map<String, Object> maps = new HashMap <String, Object>();
        maps.put("msg", "查找成功！");
        maps.put("code", 0);
        maps.put("count", totals);
        maps.put("data", topics);
        return maps;
    }
}
