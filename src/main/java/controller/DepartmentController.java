package controller;

import entity.Department;
import entity.Grade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import service.DepartmentService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author wqh
 * @date 2020/12/16 20:07
 */
@Controller
public class DepartmentController {
    @Autowired
    private DepartmentService departmentService;

    @RequestMapping("/departmentInfo")
    public String DepartmentInfo(){
        return "basicInfo/departmentInfo";
    }

    @RequestMapping("/getDepartmentInfo")
    @ResponseBody
    public Map<String,Object> getDepartmentInfo(int page,int limit,String departmentName){
        Map<String,Object> map = new HashMap<String, Object>();
        //获取当前页号
        int curr = (page-1)*limit;
        map.put("page",curr);
        map.put("limit",limit);
        map.put("departmentName",departmentName);
        List<Department> departments = departmentService.getAllDepartment(map);
        int totals = departmentService.getCountDepartment();
        Map<String, Object> maps = new HashMap <String, Object>();
        maps.put("msg", "查找成功！");
        maps.put("code", 0);
        maps.put("count", totals);
        maps.put("data", departments);
        return maps;
    }
    //获取下拉列表
    @RequestMapping("/getDpmList")
    @ResponseBody
    public Map<String,Object> getDpmList(){
        List<Department> departments = departmentService.getDpmList();
        Map<String, Object> maps = new HashMap <String, Object>();
        maps.put("msg", "查找成功！");
        maps.put("code", 0);
        maps.put("count", "");
        maps.put("data", departments);
        return maps;
    }
    //学院添加
    @RequestMapping("/addDepartment")
    public String AddDepartment(){
        return "department/addDepartment";
    }

    @RequestMapping("/insertDepartment")
    @ResponseBody
    public String insertDepartment(String departmentName,String description){
        Department department = new Department();
        department.setDepartmentName(departmentName);
        department.setDescription(description);
        String msg = departmentService.insertDepartmentInfo(department);
        return msg;
    }
    //学院修改
    @RequestMapping("/editDepartment")
    public String EditDepartment(){
        return "department/departmentEdit";
    }

    @RequestMapping("/editDpm")
    @ResponseBody
    public String editDpm(int dpm_id,String departmentName,String description){
        Department department = new Department();
        department.setId(dpm_id);
        department.setDepartmentName(departmentName);
        department.setDescription(description);
        String msg = departmentService.updateDepartmentInfo(department);
        return msg;
    }
    //学院详情
    @RequestMapping("/departmentDetail")
    public String DepartmentDetail(){
        return "department/departmentDetail";
    }

    //学院删除
    @RequestMapping("/delDepartment")
    @ResponseBody
    public String DelDepartment(int id){
        String msg = departmentService.delDeparmentInfo(id);
        return msg;
    }
}
