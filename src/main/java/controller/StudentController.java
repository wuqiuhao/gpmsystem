package controller;

import entity.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import service.StudentService;
import service.TeacherService;
import service.UserService;

import javax.servlet.http.HttpSession;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author wqh
 * @date 2020/12/16 11:33
 */
@Controller
public class StudentController {

    @Autowired
    private StudentService studentService;

    @RequestMapping("/studentList")
    public String studentInfo(){
        return "basicInfo/studentList";
    }

    //学生列表信息
    @RequestMapping("/getAllStudents")
    @ResponseBody
    public Map<String,Object> findAllStudents(int page,int limit,String studentName) {
        Map<String,Object> map = new HashMap<String, Object>();
        //获取当前页号
        int curr = (page-1)*limit;
        map.put("page",curr);
        map.put("limit",limit);
        map.put("studentName",studentName);
        Map<String,Object> maps = new HashMap<String, Object>();
        List<StudentUnit> Students = studentService.findAllStudents(map);
        int totals = studentService.getStudentCount();//查询学生总数（需要些sql语句和接口）
        maps.put("msg", "查找成功！");
        maps.put("code", 0);
        maps.put("count", totals);
        maps.put("data", Students);
        return maps;
    }
    //学生添加
    @RequestMapping("/addStudent")
    public String AddStudent(){
        return "student/addStudent";
    }

    @RequestMapping("/insertStudent")
    @ResponseBody
    public String insertStudent(String studentNo,String studentName,Integer sex,Integer majorId,Integer gradeId,Integer degree,Integer teacherId,String phone,String email,String qQ){
        Student student = new Student();
        student.setStudentNo(studentNo);
        student.setStudentName(studentName);
        student.setStudentPassword("123456");
        student.setSex(sex);
        student.setMajorId(majorId);
        student.setGrade(gradeId);
        student.setDegree(degree);
        student.setTeacherId(teacherId);
        student.setPhone(phone);
        student.setEmail(email);
        student.setQQ(qQ);
        Date date = new Date();
        student.setLastModifyTime(date);
        String msg = studentService.insertStudentInfo(student);
        return msg;
    }

    //学生修改
    @RequestMapping("/editStudent")
    public String EditStudent(){
        return "student/editStudent";
    }

    @RequestMapping("/updateStudent")
    @ResponseBody
    public String updateStudent(Integer id,String studentNo,String studentName,Integer sex,Integer majorId,Integer gradeId,Integer degree,Integer teacherId,String phone,String email,String QQ){
        Student student = new Student();
        student.setId(id);
        student.setStudentNo(studentNo);
        student.setStudentName(studentName);
        student.setSex(sex);
        student.setMajorId(majorId);
        student.setGrade(gradeId);
        student.setDegree(degree);
        student.setTeacherId(teacherId);
        student.setPhone(phone);
        student.setEmail(email);
        student.setQQ(QQ);
        Date date = new Date();
        student.setLastModifyTime(date);
        String msg = studentService.updateStudentInfo(student);
        return msg;
    }

    //删除学生
    @RequestMapping("/delStudent")
    @ResponseBody
    public String DelDepartment(Integer id){
        String msg = studentService.delStudentInfo(id);
        return msg;
    }

    //学生详情
    @RequestMapping("/studentDetail")
    public String DetailStudent(){
        return "student/studentDetail";
    }
    //我的学生
    @RequestMapping("/myStudentInfo")
    public String myStudent(){
        return "teacher/myStudentInfo";
    }
    //查询我的学生列表
    @RequestMapping("/getMyStudent")
    @ResponseBody
    public Map<String,Object> myStudentInfo(int page,int limit,String studentName,HttpSession session){
        Map<String,Object> map = new HashMap<String, Object>();
        //获取当前页号
        int curr = (page-1)*limit;
        map.put("page",curr);
        map.put("limit",limit);
        map.put("studentName",studentName);
        //当前教师所有的学生
        String  teacherNo = session.getAttribute("userNo").toString();
        map.put("teacherNo",teacherNo);
        Map<String,Object> maps = new HashMap<String, Object>();
        List<StudentUnit> studentUnits = studentService.findAllStudents(map);
        int totals = studentUnits.size();
        maps.put("msg", "查找成功！");
        maps.put("code", 0);
        maps.put("count", totals);
        maps.put("data", studentUnits);
        return maps;
    }
    //获取下拉列表
    @RequestMapping("/getMyStudentList")
    @ResponseBody
    public Map<String,Object> getMyStudentList(HttpSession session){
        String id = session.getAttribute("teacherID").toString();
        List<Student> students = studentService.getStudentList(Integer.parseInt(id));
        Map<String, Object> maps = new HashMap <String, Object>();
        maps.put("msg", "查找成功！");
        maps.put("code", 0);
        maps.put("count", "");
        maps.put("data", students);
        return maps;
    }
}
