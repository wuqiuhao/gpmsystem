package controller;

import entity.Paper;
import entity.PaperUnit;
import entity.Report;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import service.PaperService;

import javax.servlet.http.HttpSession;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author wqh
 * @date 2021/3/8 16:51
 */
@Controller
public class PaperController {
    @Autowired
    private PaperService paperService;

    @RequestMapping("/paperInfo")
    public String ReportInfo(){
        return "basicInfo/paperInfo";
    }

    @RequestMapping("/uploadPaper")
    public String uploadPaper(){
        return "paper/uploadPaper";
    }

    //课题列表controller
    @RequestMapping("/getPaperInfo")
    @ResponseBody
    public Map<String,Object> getDepartmentInfo(int page, int limit , String studentName, HttpSession session){
        Map<String,Object> map = new HashMap<String, Object>();
        //获取当前页号
        int curr = (page-1)*limit;
        map.put("page",curr);
        map.put("limit",limit);
        map.put("studentName",studentName);
        Object tid = session.getAttribute("teacherID");
        if (tid!=null){
            String t_id = tid.toString();
            map.put("tid",Integer.parseInt(t_id));
        }else {
            map.put("tid",null);
        }
        Object sid = session.getAttribute("sid");
        if (sid!=null){
            String s_id = sid.toString();
            map.put("sid",Integer.parseInt(s_id));
        }else {
            map.put("sid",null);
        }
        List<PaperUnit> papers = paperService.getAllPaperInfo(map);
        int totals = paperService.getCountPaper();
        Map<String, Object> maps = new HashMap <String, Object>();
        maps.put("msg", "查找成功！");
        maps.put("code", 0);
        maps.put("count", totals);
        maps.put("data", papers);
        return maps;
    }
    //新增论文
    @RequestMapping("/insertPaper")
    @ResponseBody
    public String inseertPaper(String paperaddress,String paperdescription,Integer s_id,Float version,String paperName){
        Paper paper = new Paper();
        paper.setIsUpload(1);
        paper.setPaperaddress(paperaddress);
        paper.setPaperdescription(paperdescription);
        Date date = new Date();
        paper.setSubmitTime(date);
        paper.setStudentId(s_id);
        paper.setVersion(version);
        paper.setPaperName(paperName);
        String msg = paperService.subPaper(paper);
        return msg;
    }
}
