package controller;

import com.alibaba.fastjson.JSONObject;
import dao.TopicDao;
import entity.Topic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import imp.UploadService;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLEncoder;
import java.util.Map;

@Controller
public class UploadController {

    @Autowired
    private UploadService uploadService;

    @Autowired
    private TopicDao topicDao;

    @RequestMapping(value = "/uploadSubmit")
    @ResponseBody
    public JSONObject uploadSubmit(MultipartFile file, String type,String id,String grade, HttpServletRequest request) {

        String realPath = request.getServletContext().getRealPath("/word");
        return uploadService.uploadWord(file, type,id,grade,realPath);
    }

    @RequestMapping("/download")
    public ResponseEntity<byte[]> downloadWord(int topicId, HttpServletRequest request) throws IOException {
        Topic topic = topicDao.downloadTaskBook(topicId);
        String taskBookPath = topic.getTaskBookPath();
        String taskBookName = topic.getTaskBookName();
        taskBookPath = taskBookPath.substring(0, taskBookPath.lastIndexOf("."));
        String realPath = request.getServletContext().getRealPath("/");
        File f = new File(realPath + taskBookPath + ".docx");
        if (f.exists()) {
           taskBookPath += ".docx";
           taskBookName += ".docx";
        } else {
            taskBookPath += ".doc";
            taskBookName += ".doc";
        }
        ServletContext context = request.getServletContext();
        InputStream in = context.getResourceAsStream(taskBookPath);
        byte[] body = new byte[in.available()];
        in.read(body);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Disposition",
                "attachment;filename=" + URLEncoder.encode(taskBookName,"utf-8"));
        HttpStatus statusCode = HttpStatus.OK;
        return new ResponseEntity<>(body, headers, statusCode);
    }
}