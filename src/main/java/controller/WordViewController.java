package controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.io.*;

@Controller
public class WordViewController {

    @ResponseBody
    @RequestMapping("/saveWord")
    public String saveWord(String contents,HttpServletRequest request, String htmlPath) {
        String savePath = request.getServletContext().getRealPath("") + htmlPath;
        try (FileOutputStream fos = new FileOutputStream(savePath);
             OutputStreamWriter osw = new OutputStreamWriter(fos, "UTF-8");) {
            osw.write(contents);
            osw.flush();
        } catch (IOException e) {
            e.printStackTrace();
            return "fail";
        }
        return "保存成功";
    }

    @RequestMapping("/wordView")
    public ModelAndView view(String path) {
        System.out.println(path);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("path",path);
        modelAndView.setViewName("/wordView/testInclude");
        return modelAndView;
    }
}
