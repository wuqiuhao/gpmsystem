package controller;

import entity.Department;
import entity.Major;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import service.MajorService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author wqh
 * @date 2021/1/10 15:33
 */
@Controller
public class MajorController {

    @Autowired
    private MajorService majorService;

    @RequestMapping("/majorInfo")
    public String majorInfo(){
        return "basicInfo/majorInfo";
    }
    //获取专业下拉列表
    @RequestMapping("/getMajorList")
    @ResponseBody
    public Map<String,Object> getDpmList(){
        Map<String, Object> maps = new HashMap <String, Object>();
        List<Major> majors = majorService.getMajorList();
        maps.put("msg", "查找成功！");
        maps.put("code", 0);
        maps.put("count", "");
        maps.put("data", majors);
        return maps;
    }
    @RequestMapping("/getMajorInfo")
    @ResponseBody
    public Map<String,Object> getMajorInfo(int page,int limit,String majorName){
        Map<String,Object> map = new HashMap<String, Object>();
        //获取当前页号
        int curr = (page-1)*limit;
        map.put("page",curr);
        map.put("limit",limit);
        map.put("majorName",majorName);
        List<Major> majors = majorService.getAllMajor(map);
        int totals = majorService.getCountMajor();
        Map<String, Object> maps = new HashMap<String, Object>();
        maps.put("msg", "查找成功！");
        maps.put("code", 0);
        maps.put("count", totals);
        maps.put("data", majors);
        return maps;
    }
    //专业详情
    @RequestMapping("/majorDetail")
    public String MajorDetail(){
        return "major/majorDetail";
    }
    //专业添加
    @RequestMapping("/addMajor")
    public String AddMajor(){
        return "major/addMajor";
    }

    @RequestMapping("/insertMajor")
    @ResponseBody
    public String insertMajor(String majorName,int departmentId,String description){
        Major major = new Major();
        major.setMajorName(majorName);
        major.setDepartmentId(departmentId);
        major.setDescription(description);
        String msg = majorService.insertMajorInfo(major);
        return msg;
    }
    //专业修改
    @RequestMapping("/editMajor")
    public String EditMajor(){
        return "major/editMajor";
    }
    @RequestMapping("/updateMajor")
    @ResponseBody
    public String updateMajor(int id,String majorName,int departmentId,String description){
        Major major = new Major();
        major.setId(id);
        major.setMajorName(majorName);
        major.setDepartmentId(departmentId);
        major.setDescription(description);
        String msg = majorService.updateMajorInfo(major);
        return msg;
    }
    //专业删除
    @RequestMapping("/delMajor")
    @ResponseBody
    public String DelMajor(int id){
        String msg = majorService.delMajorInfo(id);
        return msg;
    }
}
