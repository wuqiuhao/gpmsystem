package controller;

import entity.Student;
import entity.Teacher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;
import service.UserService;

import javax.servlet.http.HttpSession;

/**
 * @author wqh
 * @date 2020/12/3 21:53
 */
@Controller
@SessionAttributes({"userNo","password","permission"})
public class IndexController {
    @Autowired
    private UserService userService;

    //登录
    @RequestMapping("/login")
    public String login() {
        return "login/login";
    }

    @RequestMapping("/logout")
    public String logout(SessionStatus sessionStatus) {
        //清除session
        sessionStatus.setComplete();
        //返回登录
        return "login/login";
    }
    //登陆判断
    @RequestMapping("/loginSub")
    @ResponseBody
    public String isLogin(String userNo,String password,String permission, HttpSession session) {
        if (permission.equals("1")){
            int sid = userService.getStudentID(userNo);
            session.setAttribute("sid",sid);
            String msg = userService.StudentLogin(userNo,password);
            session.setAttribute("userNo",userNo);
            session.setAttribute("password",password);
            session.setAttribute("permission",permission);
            return msg;
        }
        if (permission.equals("2")){
            if(userNo.equals("189000000")){
                String msg = userService.TeacherLogin(userNo,password);
                session.setAttribute("userNo",userNo);
                session.setAttribute("password",password);
                session.setAttribute("permission",permission);
                return msg;
            }else {
                String msg = userService.TeacherLogin(userNo,password);
                int id = userService.getTeacherID(userNo);
                session.setAttribute("teacherID",id);
                session.setAttribute("userNo",userNo);
                session.setAttribute("password",password);
                session.setAttribute("permission",permission);
                msg = "1";
                return msg;
            }
        }
            return "0";
    }

    @RequestMapping("/curInfo/{userNo}/{permission}")
    public ModelAndView UserInfo(@PathVariable("userNo") String userNo,@PathVariable("permission") String permission){
        ModelAndView modelAndView = new ModelAndView();
        if (permission.equals("1")){
            //学生
            Student student = userService.getStudentInfo(userNo);
            modelAndView.setViewName("student/studentInfo");
            modelAndView.addObject("studentInfo",student);
        }
        if (permission.equals("2")){
            //教师
            Teacher teacher = userService.getTeacherInfo(userNo);
            modelAndView.setViewName("teacherList");
            modelAndView.addObject("teacherInfo",teacher);
        }
        return modelAndView;
    }
    //homepage
    @RequestMapping("/homepage")
    public String homepage() {
        return "homepage";
    }

    //welcome
    @RequestMapping("/welcome")
    public String welcome() {
        return "welcome";
    }
    //管理员主页
    @RequestMapping("/index")
    public String index() {
        return "index";
    }
    //教师主页
    @RequestMapping("/teacherIndex")
    public String teacherIndex() {
        return "teacherIndex";
    }
    //学生主页
    @RequestMapping("/studentIndex")
    public String studentIndex() {
        return "studentIndex";
    }

    //修改密码
    @RequestMapping("/updatePsw")
    public String updatePsw() {
        return "updatePsw";
    }

    @RequestMapping("/pswSum")
    @ResponseBody
    public String PswSub(String username, String password,String permission) {
        if (permission.equals("1")) {
            userService.updateStudentPsw(username,password);
            return "1";
        }
        if (permission.equals("2")) {
            userService.updateTeacherPsw(username,password);
            return "1";
        }
        return "0";
    }
}
