<%--
  Created by IntelliJ IDEA.
  User: lenovo
  Date: 2021/2/16
  Time: 12:29
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>学院详情</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/lib/layui-v2.5.5/css/layui.css">
    <script src="${pageContext.request.contextPath}/webjars/jquery/3.4.1/jquery.min.js"></script>
    <!-- 注意：如果你直接复制所有代码到本地，上述css路径需要改成你本地的 -->
</head>
<body>
<div class="site-text" style="margin: 5%;">
    <form class="layui-form" id="editDepartment" lay-filter="editDepartment">
        <div class="layui-form-item">
            <label class="layui-form-label">学院名称</label>
            <div class="layui-input-block">
                <input type="text" id="departmentName" name="departmentName" lay-verify="title" autocomplete="off"
                       placeholder="请输入学院名称" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item layui-form-text">
            <label class="layui-form-label">学院描述</label>
            <div class="layui-input-block">
                <textarea placeholder="请输入学院描述" class="layui-textarea" id="description" name="description"></textarea>
            </div>
        </div>
    </form>
</div>
<script src="${pageContext.request.contextPath}/static/lib/layui-v2.5.5/layui.js" charset="utf-8"></script>
<!-- 注意：如果你直接复制所有代码到本地，上述js路径需要改成你本地的 -->
<script>
    layui.use(['form','jquery'], function() {
        const form = layui.form
            , layer = layui.layer
            , $ = layui.jquery;
        form.val("editDepartment", {
            "departmentName": parent.departmentName,
            "description": parent.description,
        })
    })
</script>
</body>
</html>
