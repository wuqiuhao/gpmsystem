<%--
  Created by IntelliJ IDEA.
  User: lenovo
  Date: 2021/3/16
  Time: 11:30
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>我的课题</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/lib/layui-v2.5.5/css/layui.css">
    <script src="${pageContext.request.contextPath}/webjars/jquery/3.4.1/jquery.min.js"></script>
    <!-- 注意：如果你直接复制所有代码到本地，上述css路径需要改成你本地的 -->
</head>
<body>
<table class="layui-hide" id="myTopic" lay-filter="myTopic"></table>
<script type="text/html" id="barDemo">
    {{#  if(d.is_upload== "0"){ }}
    <a class="layui-btn layui-btn-disabled layui-btn-sm" lay-event="download">未上传任务书</a>
    {{#  } }}
    {{#  if(d.is_upload != "0"){ }}
    <a class="layui-btn layui-btn-xs" lay-event="download">下载任务书</a>
    {{#  } }}
    <a class="layui-btn layui-btn-xs" lay-event="upload">上传开题报告</a>
</script>
<script src="${pageContext.request.contextPath}/static/lib/layui-v2.5.5/layui.js" charset="utf-8"></script>
<!-- 注意：如果你直接复制所有代码到本地，上述js路径需要改成你本地的 -->
<script>
    layui.use('table', function() {
        var table = layui.table
            , layer = layui.layer
            , $ = layui.jquery;
        table.render({
            elem: '#myTopic'
            , id: 'myTopicTable'
            , url: '${pageContext.request.contextPath}/myTopicInfo'//接收后端数据
            , toolbar: '#toolbarDemo' //开启头部工具栏，并为其绑定左侧模板
            , title: '教师信息表'
            , cols: [[
                {type: 'checkbox', fixed: 'left'}
                , {field: 'id', title: 'ID', width: 100, fixed: 'left', unresize: true, sort: true}
                , {field: 'thesisName', title: '课题名称', width: 220, edit: 'text'}
                , {field: 'teacherId', title: '教师ID', width: 120,hide: true}
                , {field: 's_id', title: '此课题是否被选', width: 180,templet:function (d) {
                        if (d.s_id == 0) return '未被选'
                        else return '已被选'
                    }}
                , {field: 's_name', title: '学生姓名', width: 200}
                , {field: 'grade', title: '年级', width: 200}
                , {field: 'taskBookPath', title: '任务书地址', width: 250,hide: true}
                , {field: 'is_upload', title: '状态', width: 130,templet:function (d) {
                        if (d.is_upload == 0) return '未上传任务书'
                        else return '已上传任务书'
                    }}
                , {title: '操作', toolbar: '#barDemo', width: 400}
            ]]
            , page: true
        });

        //监听行工具事件
        table.on('tool(myTopic)', function(obj){
            var data = obj.data;
            id = data.id;
            grade = data.grade;
            //taskBookName = data.taskBookName;
            thesisName = data.thesisName;
            if(obj.event === 'download') {
                window.location.href = "${pageContext.request.contextPath}/download?topicId="+data.id;
            }else if(obj.event === 'upload') {
                layer.open({
                    type: 2,
                    skin: 'layui-layer-rim',
                    area: ['60%', '70%'],
                    content: '${pageContext.request.contextPath}/uploadReport' //调到新增页面
                })
            }
    })
    })
</script>
</body>
</html>
