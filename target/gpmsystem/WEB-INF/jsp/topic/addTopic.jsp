<%--
  Created by IntelliJ IDEA.
  User: lenovo
  Date: 2021/3/14
  Time: 21:40
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>添加课题</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/lib/layui-v2.5.5/css/layui.css">
    <script src="${pageContext.request.contextPath}/webjars/jquery/3.4.1/jquery.min.js"></script>
    <!-- 注意：如果你直接复制所有代码到本地，上述css路径需要改成你本地的 -->
</head>
<body>
<div class="site-text" style="margin: 5%;">
    <form class="layui-form" id="insertTopic" lay-filter="insertMajor">
        <div class="layui-form-item">
            <label class="layui-form-label">课题名称</label>
            <div class="layui-input-block">
                <input type="text" id="thesisName" name="thesisName" lay-verify="title" autocomplete="off"
                       placeholder="请输入课题名称" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item" class="layui-form">
            <label class="layui-form-label">分配学生</label>
            <div class="layui-input-block">
                <select id="s_id" name="s_id" lay-filter="s_id">
                    <option value=""></option>
                </select>
            </div>
        </div>
        <div class="layui-form-item" style="margin-top:40px">
            <div class="layui-input-block">
                <button lay-submit="" id="addTopic" class="layui-btn layui-btn-normal layui-btn-radius">添加课题</button>
            </div>
        </div>
    </form>
</div>
<script src="${pageContext.request.contextPath}/static/lib/layui-v2.5.5/layui.js" charset="utf-8"></script>
<!-- 注意：如果你直接复制所有代码到本地，上述js路径需要改成你本地的 -->
<script>
    layui.use(['form','upload'], function() {
        const form = layui.form
            , layer = layui.layer
            ,upload = layui.upload
            , $ = layui.jquery;
        //分配学生
        $.ajax({
            url: '${pageContext.request.contextPath}/getMyStudentList',//获取学院列表信息
            dataType: 'json',
            type: 'get',
            success: function (data) {
                //使用循环遍历，给下拉列表赋值
                $.each(data.data, function (index, value) {
                    $('#s_id').append(new Option(value.studentName,value.id));// 下拉菜单里添加元素
                    $('#s_id').find('option').each(function() {
                        //$(this).attr('selected', $(this).val() == departmentID);
                    });
                });
                layui.form.render("select");//重新渲染 固定写法
            }
        })
        //监听submit提交按钮 button ，lay-filter 为 noticeAdd 的
        $('#addTopic').on('click',function (){
            $.ajax({
                type: "POST",
                dataType: "text",
                url: "${pageContext.request.contextPath}/insertTopic",
                data: $('#insertTopic').serialize(),  //表单数据
                success: function (result) {
                    result = JSON.parse(result);
                    if (result === "1") {
                        layer.msg('添加成功，1秒后自动关闭该窗口');
                        //延迟1秒执行，目的是让用户看到提示
                        setTimeout(function () {
                            //1、先得到当前iframe层（弹出层）的索引  ///2、提交成功关闭弹出层窗口
                            const index = layer.getFrameIndex(window.name);
                            layer.close(index);
                            window.parent.location.reload(); //刷新父页面
                        }, 1 * 1000);
                    }else {
                        layer.msg(result);
                    };
                },
                error: function () {
                    layer.msg('后台异常！未添加成功');
                }
            });
            //阻止页面跳转
            return false;
        });
    })
</script>
</body>
</html>
