<%--
  Created by IntelliJ IDEA.
  User: lenovo
  Date: 2020/12/21
  Time: 21:47
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>修改密码</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/lib/layui-v2.5.5/css/layui.css" media="all">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/public.css" media="all">
    <script src="${pageContext.request.contextPath}/webjars/jquery/3.4.1/jquery.min.js"></script>
    <style>
        .layui-form-item .layui-input-company {width: auto;padding-right: 10px;line-height: 38px;}
    </style>
</head>
<body>
<div class="layuimini-container">
    <div class="layuimini-main">

        <div class="layui-form layuimini-form">
            <div class="layui-form-item">
                <label class="layui-form-label required">旧的密码</label>
                <div class="layui-input-block">
                    <input type="password" id="old_password" name="old_password" lay-verify="required" lay-reqtext="旧的密码不能为空" placeholder="请输入旧的密码"  value="" class="layui-input">
                    <tip>填写自己账号的旧的密码。</tip>
                </div>
            </div>

            <div class="layui-form-item">
                <label class="layui-form-label required">新的密码</label>
                <div class="layui-input-block">
                    <input type="password" id="new_password" name="new_password" lay-verify="required" lay-reqtext="新的密码不能为空" placeholder="请输入新的密码"  value="" class="layui-input">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label required">新的密码</label>
                <div class="layui-input-block">
                    <input type="password" id="again_password" name="again_password" lay-verify="required" lay-reqtext="新的密码不能为空" placeholder="请输入新的密码"  value="" class="layui-input">
                </div>
            </div>

            <div class="layui-form-item">
                <div class="layui-input-block">
                    <button class="layui-btn layui-btn-normal" lay-submit lay-filter="saveBtn" id="updatePassword">确认保存</button>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="${pageContext.request.contextPath}/static/lib/layui-v2.5.5/layui.js" charset="utf-8"></script>
<script src="${pageContext.request.contextPath}/static/js/lay-config.js?v=1.0.4" charset="utf-8"></script>
<script>
    layui.use(['form','miniTab','layer'], function () {
        var form = layui.form,
            layer = layui.layer,
            miniTab = layui.miniTab;

        //监听提交
        $('#updatePassword').click(function (){
            //判断原密码和新密码是否符合要求
            username = ${sessionScope.userNo};
            org_password = ${sessionScope.password};
            old_password = $('#old_password').val();
            new_password = $('#new_password').val();
            again_password = $('#again_password').val();
            if(org_password != old_password){
                layer.msg("原密码输入不正确！");
            }else if(new_password != again_password)
            {
                layer.msg("新密码两次输入不一致！");
            }else if (old_password == again_password)
            {
                layer.msg("新密码不能与原密码相同！");
            }else {
            $.ajax({
                type: "POST",
                dataType: "text",
                url: "${pageContext.request.contextPath}/pswSum",
                data: {"password":again_password,"username":username,"permission":${sessionScope.permission}},  //表单数据
                success: function (result) {
                    result = JSON.parse(result);
                    if (result === "1") {
                        layer.msg('密码修改成功，1秒后自动关闭该窗口');
                        //延迟1秒执行，目的是让用户看到提示
                        setTimeout(function () {
                            top.location.href = '${pageContext.request.contextPath}/logout';
                        }, 1 * 1000);
                    }else {
                        layer.msg('密码修改失败！');
                    };
                },
                error: function () {
                    layer.msg('后台异常！未添加成功');
                }
            });
            //阻止页面跳转
            return false;
            }
        });
    });
</script>
</body>
</html>
