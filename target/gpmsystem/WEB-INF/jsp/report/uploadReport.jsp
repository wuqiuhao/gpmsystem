<%--
  Created by IntelliJ IDEA.
  User: lenovo
  Date: 2021/3/16
  Time: 13:29
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>上传开题报告</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/lib/layui-v2.5.5/css/layui.css">
    <script src="${pageContext.request.contextPath}/webjars/jquery/3.4.1/jquery.min.js"></script>
    <!-- 注意：如果你直接复制所有代码到本地，上述css路径需要改成你本地的 -->
</head>
<body>
<div class="site-text" style="margin: 5%;">
    <form class="layui-form" id="uploadReport" lay-filter="uploadReport">
        <div class="layui-form-item" style="display: none">
            <label class="layui-form-label">课题ID</label>
            <div class="layui-input-block">
                <input type="text" id="id" name="id" lay-verify="title" autocomplete="off"
                       class="layui-input">
            </div>
        </div>
        <div class="layui-form-item" style="display: none">
            <label class="layui-form-label">版本</label>
            <div class="layui-input-block">
                <input type="text" id="version" name="version" lay-verify="title" autocomplete="off"
                       class="layui-input">
            </div>
        </div>
        <div class="layui-form-item" style="display: none">
            <label class="layui-form-label">开题报告地址</label>
            <div class="layui-input-block">
                <input type="text" id="reportAddress" name="reportAddress" lay-verify="title" autocomplete="off"
                       class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">课题名称</label>
            <div class="layui-input-block">
                <input type="text" id="thesisName" name="thesisName" lay-verify="title" autocomplete="off"
                       class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">描述</label>
            <div class="layui-input-block">
                <textarea placeholder="请输入开题报告描述" class="layui-textarea" id="description" name="description"></textarea>
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">上传开题报告</label>
            <div class="layui-input-block">
                <button type="button" class="layui-btn" id="selectFile"><i class="layui-icon"></i>选择文件</button>
                <button type="button" class="layui-btn" id="upload">开始上传</button>
            </div>
        </div>
        <div class="layui-form-item" style="margin-top:40px">
            <div class="layui-input-block">
                <button lay-submit="" id="uploadR" class="layui-btn layui-btn-normal layui-btn-radius">上传开题报告</button>
            </div>
        </div>
    </form>
</div>
<script src="${pageContext.request.contextPath}/static/lib/layui-v2.5.5/layui.js" charset="utf-8"></script>
<!-- 注意：如果你直接复制所有代码到本地，上述js路径需要改成你本地的 -->
<script>
    layui.use(['form','upload'], function() {
        const form = layui.form
            , layer = layui.layer
            ,upload = layui.upload
            , $ = layui.jquery;
        form.val("uploadReport", {
            "id":parent.id,
            "thesisName":parent.thesisName,
        })
        //指定文件上传及上传类型
        upload.render({ //允许上传的文件后缀
            elem: '#selectFile'
            ,url: '${pageContext.request.contextPath}/uploadSubmit' //改成您自己的上传接口
            ,accept: 'file' //普通文件
            ,exts: 'docx|doc' //只允许上传word
            ,auto: false
            //,multiple: true
            ,bindAction: '#upload'
            //上传前的回调
            ,before: function () {
                this.data = {
                    type: 'report',
                    id:parent.id,
                    grade:parent.grade,
                }
            }
            //操作成功的回调
            ,done: function (res, index, upload) {
                if (res.code === 1){
                    $('#reportAddress').val(res.data);
                    $('#version').val(res.msg);
                    layer.msg('上传成功!')
                }else {
                    layer.msg('上传失败!')
                }
            }
        });
        //监听submit提交按钮 button ，lay-filter 为 noticeAdd 的
        $('#uploadR').on('click',function (){
            $.ajax({
                type: "POST",
                dataType: "text",
                url: "${pageContext.request.contextPath}/addReport",
                data: $('#uploadReport').serialize(),  //表单数据
                success: function (result) {
                    result = JSON.parse(result);
                    if (result === "1") {
                        layer.msg('上传成功，1秒后自动关闭该窗口');
                        //延迟1秒执行，目的是让用户看到提示
                        setTimeout(function () {
                            //1、先得到当前iframe层（弹出层）的索引  ///2、提交成功关闭弹出层窗口
                            const index = parent.layer.getFrameIndex(window.name);
                            parent.layer.close(index);
                            window.parent.location.reload(); //刷新父页面
                        }, 1 * 1000);
                    }else {
                        layer.msg(result);
                    };
                },
                error: function () {
                    layer.msg('后台异常！未上传成功');
                }
            });
            //阻止页面跳转
            return false;
        });
    })
</script>
</body>
</html>
