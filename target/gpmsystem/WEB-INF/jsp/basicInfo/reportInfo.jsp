<%--
  Created by IntelliJ IDEA.
  User: lenovo
  Date: 2021/3/7
  Time: 22:19
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8" %>
<html>
<head>
    <title>开题报告列表</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/lib/layui-v2.5.5/css/layui.css">
    <script src="${pageContext.request.contextPath}/webjars/jquery/3.4.1/jquery.min.js"></script>
    <!-- 注意：如果你直接复制所有代码到本地，上述css路径需要改成你本地的 -->
</head>
<body>
<blockquote class="layui-elem-quote news_search">
    <div class="layui-inline">
        <div class="layui-input-inline">
            <input type="text"  id="searchInfo" name="searchInfo" value="" placeholder="请输入课题名称" class="layui-input search_input">
        </div>
        <button class="layui-btn" data-type="reload" id="reload">搜索</button>
    </div>
</blockquote>
<table class="layui-hide" id="reportInfo" lay-filter="reportInfo"></table>
<script type="text/html" id="zizeng">
    {{d.LAY_TABLE_INDEX+1}}
</script>
<script type="text/html" id="barDemo">
    {{#  if(${sessionScope.permission}== "2"){ }}
    <a class="layui-btn layui-btn-xs" lay-event="detail">查看</a>
    <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="edit">打分</a>
    {{#  } }}
    {{#  if(${sessionScope.permission}== "1"){ }}
    <a class="layui-btn layui-btn-xs" lay-event="detail">查看</a>
    <a class="layui-btn layui-btn-xs" lay-event="upload">上传论文</a>
    {{#  } }}
</script>
<script src="${pageContext.request.contextPath}/static/lib/layui-v2.5.5/layui.js" charset="utf-8"></script>
<!-- 注意：如果你直接复制所有代码到本地，上述js路径需要改成你本地的 -->
<script>
    layui.use('table', function() {
        var table = layui.table
            , layer = layui.layer
            , $ = layui.jquery;
        table.render({
            elem: '#reportInfo'
            ,id:'reportTable'
            , url: '${pageContext.request.contextPath}/getReportInfo'//接收后端数据
            , toolbar: '#toolbarDemo' //开启头部工具栏，并为其绑定左侧模板
            , title: '开题报告信息表'
            , cols: [[
                {type: 'checkbox', fixed: 'left'}
                ,{field:'zizeng', width:80, title: '序号',fixed: 'left',templet:'#zizeng'}
                , {field: 'id', title: 'ID', width: 100, hide:true}
                , {field: 'studentName', title: '学生姓名', width: 200}
                , {field: 'sid', title: '学生ID', width: 100,hide:true}
                , {field: 'grade', title: '年级', width: 100}
                , {field: 'thesisName', title: '课题名称', width: 250}
                , {field: 'isSubmit', title: '状态', width: 160,templet:function (d) {
                        if (d.isSubmit == 0) return '未提交'
                        else return '已提交'
                    }}
                , {field: 'studentScore', title: '学生成绩', width: 110}
                , {field: 'rater', title: '评分人', width: 150 }
                /*, {field: 'rank', title: '等级', width: 120,templet:function (d) {
                        if (0 <= d.rank <= 59) return '不及格'
                        else if (60 <= d.rank <= 100) return '及格'
                    }}*/
                , {field: 'version', title: '版本', width: 80}
                , {field: 'reportAddress', title: '地址', width: 80, hide:true}
                , {title: '操作', toolbar: '#barDemo', width: 308}
            ]]
            , page: true
        });
        //监听查询
        var $=layui.$,active={
            reload:function(){
                //执行重载
                table.reload('reportTable',{
                    method:'post',
                    where:{
                        thesisName:$('#searchInfo').val(),
                    }
                });
            }
        };
        $('#reload').on('click', function(){
            var type = $(this).data('type');
            active[type] ? active[type].call(this) : '';
        });
        //监听行工具事件
        table.on('tool(reportInfo)', function(obj){
            var data = obj.data;
            id = data.id;
            sid = data.sid;
            studentName = data.studentName;
            thesisName = data.thesisName;
            studentScore = data.studentScore;
            grade = data.grade;
            rater = data.rater;
            if (obj.event === 'edit') {
                layer.open({
                    type: 2,
                    skin: 'layui-layer-rim',
                    area: ['60%', '70%'],
                    content: '${pageContext.request.contextPath}/reportScoring' //调到编辑页面
                })
            } else if (obj.event === 'detail') {
                var url = "${pageContext.request.contextPath}/wordView";	//请求路径
                var type = "POST";	//请求类型
                var jsonData = [];	//构建json
                jsonData.push({"path":data.reportAddress});	//将传参放入json中
                var targetType = "_self";	//在当前页面打开
                formSubmit(url,type,jsonData,targetType)
            } else if (obj.event === 'upload') {
                layer.open({
                    type: 2,
                    skin: 'layui-layer-rim',
                    area: ['60%', '70%'],
                    content: '${pageContext.request.contextPath}/uploadPaper' //调到详情页面
                })
            }
        });
    })
    function formSubmit(url,type,jsonData,targetType) {
        var dlform = document.createElement('form');
        dlform.style = "display:none;";
        dlform.method = type;
        dlform.action = url;
        dlform.target = targetType
        var json = jsonData;
        for(var i=0,l=json.length;i<l;i++){
            for(var key in json[i]){
                var hdnFilePath = document.createElement('input');
                hdnFilePath.type = 'hidden';
                hdnFilePath.name = key;
                hdnFilePath.value = json[i][key];
            }
        }
        dlform.appendChild(hdnFilePath);
        document.body.appendChild(dlform);
        dlform.submit();
        document.body.removeChild(dlform);
    }
</script>
</body>
</html>
