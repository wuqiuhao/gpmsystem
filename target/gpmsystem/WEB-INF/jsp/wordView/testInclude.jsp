<%--
  Created by IntelliJ IDEA.
  User: 夏兵
  Date: 2020/11/18
  Time: 10:05
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title></title>
    <!-- 新 Bootstrap4 核心 CSS 文件 -->
    <link rel="stylesheet" href="webjars/bootstrap/4.5.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/onlineText.css"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/lib/layui-v2.5.5/css/layui.css">
</head>
<script src="webjars/jquery/3.4.1/jquery.min.js"></script>
<script src="webjars/popper.js/1.16.0/popper.js"></script>
<script src="webjars/bootstrap/4.5.2/js/bootstrap.min.js"></script>
<script src="${pageContext.request.contextPath}/static/js/wordComments.js"></script>
<script src="${pageContext.request.contextPath}/static/lib/layui-v2.5.5/layui.js" charset="utf-8"></script>
<!-- 注意：如果你直接复制所有代码到本地，上述js路径需要改成你本地的 -->
<script>
    const htmlPath = '${pageContext.request.contextPath}' + '${path}';   // 查看的html地址
    let tag = false // 标记为true表示已经保存过一次
    $(function (){
        var bodyContent = $.ajax({
            url: htmlPath,
            global: false,
            type: "GET",
            dataType: "html",
            async: false,
            success: function (msg) {
                $("#newPage").html(msg);
            }
        })
        let realPath = htmlPath.substring(0, htmlPath.lastIndexOf("/") + 1)
        $('#newPage img').each(function () {
            let imgUrl = $(this).attr('src')
            if (imgUrl.indexOf('word') !== -1) {
                tag = true
                return false
            }
            $(this).attr('src', realPath + '/' + imgUrl)
        })
        $('#newPage link').each(function () {
            let linkUrl = $(this).attr('href')
            if (tag) {
                return false
            }
            $(this).attr('href', realPath + '/' + linkUrl)
        })
        if (!tag) {
            let left = $('<div style="float: left; width: 75%"></div>')
            let newPage = $('#newPage')
            newPage.html(left.append(newPage.html()))
            newPage.append($('<div style="right: 10px; position: fixed; top: 50px; width: 25%" id="note"></div>'))
        }
    })
    function save() {
        let contents = $("#newPage").html()
        let param = {contents: contents, htmlPath: '${path}'}
        let url = '${pageContext.request.contextPath}/saveWord'
        $.post(url, param, function (data) {
            if (data === 'fail') {
                alert('保存失败')
            } else {
                alert('保存成功')
                location.reload()
            }
        })
    }
</script>
<body ${permission != '1' ? "oncontextmenu='rightClick()'" : ''}>
<c:if test="${permission != '1'}">
    <button type="button" id="saveText" class="layui-btn" style="right: 1px;position: fixed;float: right" onclick="save()">
        save</button>
</c:if>
<button style="position: fixed" type="button" class="layui-btn" onclick="window.history.back();">
    back</button>
<jsp:include page="oncontextMenu.jsp"/>
<div id = "allElements">
    <div id="newPage">
    </div>
</div>
</body>
</html>
